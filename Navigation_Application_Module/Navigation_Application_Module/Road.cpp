#include "Road.h"

vector<Road> Road::Road_container;

Road::Road(POI origem, POI destino) {

	this->poi_origem_id = origem.getID();
	this->poi_destino_id = destino.getID();

	/*First POI data*/
	this->a.center.x = origem.getX();
	this->a.center.y = origem.getY();
	this->a.center.z = origem.getZ();
	this->a.radius = origem.getRadius();

	/*Second POI data.*/
	this->b.center.x = destino.getX();
	this->b.center.y = destino.getY();
	this->b.center.z = destino.getZ();
	this->b.radius = destino.getRadius();

	//computePOIsDistance();
	//computePOIsAngle();
	computeLineEquation();
}

void Road::render() {
	glPushMatrix();
	glEnable(GL_COLOR_MATERIAL);
	glColor3f(0.2f, 0.3f, 0.5f);
	glPushMatrix();
	//glColor3f(0.5f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->a.p1.x, this->a.p1.y, this->a.p1.z - 0.1f); //this->a.p1.z
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->a.p3.x, this->a.p3.y, this->a.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->a.p4.x, this->a.p4.y, this->a.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->a.p2.x, this->a.p2.y, this->a.p1.z - 0.1f);
	glEnd();

	//glColor3f(0.1f, 0.5f, 0.1f);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->b.p1.x, this->b.p1.y, this->b.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->b.p3.x, this->b.p3.y, this->b.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->b.p4.x, this->b.p4.y, this->b.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->b.p2.x, this->b.p2.y, this->b.p1.z - 0.1f);
	glEnd();

	//glColor3f(0.1f, 0.1f, 0.5f);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->a.p3.x, this->a.p3.y, this->a.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->a.p4.x, this->a.p4.y, this->a.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->b.p4.x, this->b.p4.y, this->b.p1.z - 0.1f);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(this->b.p3.x, this->b.p3.y, this->b.p1.z - 0.1f);
	glEnd();

	/*Contornos.*/
	/*glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(this->a.p1.x, this->a.p1.y, 0.0f);
	glVertex3f(this->a.p2.x, this->a.p2.y, 0.0f);
	glVertex3f(this->b.p2.x, this->b.p2.y, 0.0f);
	glVertex3f(this->b.p1.x, this->b.p1.y, 0.0f);
	glEnd();*/

	glColor4f(1.0, 1.0, 1.0, 1.0);
	glDisable(GL_COLOR_MATERIAL);
	glPopMatrix();
	glPopMatrix();
}

inline float
isLeft(Point P0, Point P1, Point P2)
{
	return ((P1.x - P0.x) * (P2.y - P0.y)
		- (P2.x - P0.x) * (P1.y - P0.y));
}

void Road::computePOIsDistance() {
	this->distance_between_pois = sqrtf(pow((this->b.center.x - this->a.center.x), 2) + (pow((this->b.center.y - this->a.center.y), 2)));
}

void Road::computePOIsAngle() {
	GLfloat x_variation = a.center.x - b.center.x;
	x_variation = abs(x_variation);

	GLfloat v = x_variation / distance_between_pois;
	this->angle_between_pois = acos(v);
}

void Road::computeLineEquation() {
	/*For any dot.*/
	//y = mx + c;

	/*verificar se b.center.y = a.center.x*/
	GLfloat m, d, inv_m, k, k2, distance_a, distance_b, new_plus_x, new_plus_y;

	/*if (this->poi_origem_id == 64) {
	}
	if (a.center.x == b.center.x) {
	}
	if (b.center.y == a.center.y) {
	}*/

	this->poi_origem_id;
	this->poi_destino_id;

	if (this->poi_destino_id == 78 || this->poi_origem_id == 78) {
		cout << endl;
	}
	m = (b.center.y - a.center.y) / (b.center.x - a.center.x); // calcular a equa��o da reta entre os pois.

	if (m < 0.05 && m > -0.05) {
		// reta � paralela ao eixo dos x's
		// x =  b.center.y || x = a.center.x
		GLfloat x = a.center.x;
		compute2DegreeEquationNoDeclive(x, a.center.x, a.center.y, a.p1, a.p2, distance_a);
		GLfloat x2 = b.center.x;
		compute2DegreeEquationNoDeclive(x2, b.center.x, b.center.y, b.p1, b.p2, distance_b);

		compute_middle_dot_no_declive(a.p1.x, a.p1.y, this->a.radius, new_plus_x, new_plus_y);
		a.p3.x = new_plus_x;
		a.p3.y = new_plus_y;

		new_plus_x = NULL;
		new_plus_y = NULL;
		compute_middle_dot_no_declive(a.p2.x, a.p2.y, this->a.radius, new_plus_x, new_plus_y);
		a.p4.x = new_plus_x;
		a.p4.y = new_plus_y;

		new_plus_x = NULL;
		new_plus_y = NULL;
		compute_middle_dot_no_declive(b.p1.x, b.p1.y, this->b.radius, new_plus_x, new_plus_y);
		b.p3.x = new_plus_x;
		b.p3.y = new_plus_y;

		new_plus_x = NULL;
		new_plus_y = NULL;
		compute_middle_dot_no_declive(b.p2.x, b.p2.y, this->b.radius, new_plus_x, new_plus_y);
		b.p4.x = new_plus_x;
		b.p4.y = new_plus_y;

	}
	else {
		d = a.center.y - (m * a.center.x);
		inv_m = -1.0f / m; //sim�trico de m para retas prependiculares.
		k = a.center.y - (inv_m * a.center.x); //coeficiente linear da reta prependicular que passa no primeiro poi.
		k2 = b.center.y - (inv_m * b.center.x); //coeficiente linear da reta prependicular que passa no segundo poi.
											//y = inv_m * x + k;
		//compute2DegreeEquation(inv_m, k, a.center.x, a.center.y, a.p1, a.p2,distance_a);
		//compute2DegreeEquation(inv_m, k2, b.center.x, b.center.y, b.p1, b.p2,distance_b);

		/* As dist�ncias est�o a ser valores muito pr�ximos mas diferentes.*/
		//distance_a;
		//distance_b;

		/*Onde o valor incial � o centro.*/
		GLfloat new_x = a.center.x;
		GLfloat new_y;

		computeRoadVertex(new_x, new_y, inv_m, k, a.center.x, a.center.y, 0.05); //valor a incrementar.
		a.p1.x = new_x;
		a.p1.y = new_y;

		compute_middle_dot(a.p1.x, a.p1.y, m, this->a.radius, new_plus_x, new_plus_y);
		a.p3.x = new_plus_x; //este ponto vai connectar com a.p1;
		a.p3.y = new_plus_y;

		new_x = NULL;
		new_y = NULL;
		new_plus_x = NULL;
		new_plus_y = NULL;
		new_x = a.center.x;

		computeRoadVertex(new_x, new_y, inv_m, k, a.center.x, a.center.y, -0.05);
		a.p2.x = new_x;
		a.p2.y = new_y;

		compute_middle_dot(a.p2.x, a.p2.y, m, this->a.radius, new_plus_x, new_plus_y);
		a.p4.x = new_plus_x; //este ponto vai connectar com a.p1;
		a.p4.y = new_plus_y;

		//distance_a = sqrtf((pow((a.p2.x - a.p1.x), 2)) + (pow((a.p2.y - a.p1.y), 2)));

		new_x = NULL;
		new_y = NULL;
		new_plus_x = NULL;
		new_plus_y = NULL;
		new_x = b.center.x;

		new_x = b.center.x;
		computeRoadVertex(new_x, new_y, inv_m, k2, b.center.x, b.center.y, 0.05);
		b.p1.x = new_x;
		b.p1.y = new_y;

		compute_middle_dot(b.p1.x, b.p1.y, m, this->b.radius, new_plus_x, new_plus_y);
		b.p3.x = new_plus_x; //este ponto vai connectar com a.p1;
		b.p3.y = new_plus_y;

		new_x = NULL;
		new_y = NULL;
		new_plus_x = NULL;
		new_plus_y = NULL;
		new_x = b.center.x;

		new_x = b.center.x;
		computeRoadVertex(new_x, new_y, inv_m, k2, b.center.x, b.center.y, -0.05);
		b.p2.x = new_x;
		b.p2.y = new_y;

		compute_middle_dot(b.p2.x, b.p2.y, m, this->b.radius, new_plus_x, new_plus_y);
		b.p4.x = new_plus_x; //este ponto vai connectar com a.p1;
		b.p4.y = new_plus_y;

		//distance_b = sqrtf((pow((b.p2.x - b.p1.x), 2)) + (pow((b.p2.y - b.p1.y), 2)));
	}
	/*Comentar at� aqui.*/
	a.p1.z = a.center.z;
	a.p2.z = a.center.z;
	b.p1.z = b.center.z;
	b.p2.z = b.center.z;

}

void Road::compute2DegreeEquationNoDeclive(GLfloat x, GLfloat a, GLfloat b, Point &sol1, Point &sol2, GLfloat &distance) {

	GLfloat c2 = (GLfloat)(pow((x - a), 2)) - (pow(RADIUS, 2));
	GLfloat c3 = (GLfloat)(pow(b, 2));
	GLfloat c_value = c2 + c3;
	GLfloat b_value = (GLfloat)(-2.0f * b);
	GLfloat a_value = 1.0f;

	GLfloat delta = (pow(b_value, 2)) - (4.0f * a_value * c_value);

	sol1.x = x;
	sol1.y = ((-b_value) + sqrtf(delta)) / (2 * a_value);
	sol2.x = x;
	sol2.y = ((-b_value) - sqrtf(delta)) / (2 * a_value);

	distance = sqrtf((pow((sol2.x - sol1.x), 2)) + (pow((sol2.y - sol1.y), 2)));

}

void Road::compute2DegreeEquation(GLfloat inv_m, GLfloat k, GLfloat a, GLfloat b, Point &sol1, Point &sol2, GLfloat &distance) {
	// (x - a)^2 + ((inv_m * x - k) - b)^2 - 1^2 = 0;

	/*Forma2*/
	//GLfloat c1 = pow(a, 2);
	//GLfloat b1 = -2 * a;
	//GLfloat a1 = 1;
	//GLfloat c2 = pow(b, 2);
	//GLfloat c3 = -2 * k * b;
	//GLfloat b2 = -2 * inv_m * b;
	//GLfloat a2 = pow(inv_m, 2);
	//GLfloat b3 = 2 * inv_m * k;
	//GLfloat c4 = pow(k, 2);
	//GLfloat a_value = a1 + a2;
	//GLfloat b_value = b1 + b2 + b3;
	//GLfloat c_value = c1 + c2 + c3 + c4 - pow(RADIUS, 2);

	/*Forma1*/
	GLfloat a_value = (GLfloat)(pow(inv_m, 2) + 1);// valor de a para ax^2 
	GLfloat b_value = (GLfloat)((-2.0f * a) + (2.0f * inv_m * (k - b)));
	GLfloat c_value = (GLfloat)(pow(a, 2) + pow((k - b), 2) - pow(RADIUS, 2));

	GLfloat delta = (pow(b_value, 2)) - (4.0f * a_value * c_value);

	if (delta < 0) {
		complex<float> x_sol_1 = ((-b_value) + sqrt(complex<float>(delta, 0))) / (2.0f * a_value);
		float real = x_sol_1.real();
		float imag = x_sol_1.imag();
		sol1.x = sqrtf(pow(real, 2) + pow(imag, 2));
		sol1.y = computeYBasedonXPosition(sol1.x, inv_m, k);

		complex<float> x_sol_2 = ((-b_value) - sqrt(complex<float>(delta, 0))) / (2.0f * a_value);
		float real2 = x_sol_2.real();
		float imag2 = x_sol_2.imag();
		sol2.x = sqrtf(pow(real2, 2) - pow(imag2, 2));
		sol2.y = computeYBasedonXPosition(sol2.x, inv_m, k);

		distance = sqrtf((pow((sol2.x - sol1.x), 2)) + (pow((sol2.y - sol1.y), 2)));
	}
	else {
		sol1.x = ((-b_value) + sqrtf(delta)) / (2.0f * a_value);
		sol1.y = computeYBasedonXPosition(sol1.x, inv_m, k);

		sol2.x = ((-b_value) - sqrtf(delta)) / (2.0f  * a_value);
		sol2.y = computeYBasedonXPosition(sol2.x, inv_m, k);

		distance = sqrtf((pow((sol2.x - sol1.x), 2)) + (pow((sol2.y - sol1.y), 2)));

	}
}

GLfloat Road::computeYBasedonXPositionNoDeclive(GLfloat x, GLfloat y) {
	return x;
}

GLfloat Road::computeYBasedonXPosition(GLfloat x, GLfloat m, GLfloat k) {
	return m * x + k;
}

void Road::computeRoadVertex(GLfloat &x, GLfloat &y, GLfloat m, GLfloat k, GLfloat a, GLfloat b, GLfloat sum_rate) {
	while (true) {
		y = m * x + k;
		if (checkInterceptionWithBase(x, y, a, b)) {
			break;
		}
		x += sum_rate;
	}
}

void Road::compute_middle_dot(GLfloat a, GLfloat b, GLfloat m, GLfloat radius, GLfloat &x, GLfloat &y) {


	GLfloat angle = atan(m); //this is the angle with the x-axys;

	GLfloat plus_x = cos(angle) * radius;
	GLfloat plus_y = sin(angle) * radius;

	/*x = a + plus_x;
	y = b + plus_y;*/

	/*Checks Origin POI position.*/
	if (this->a.p1.x == a || this->a.p2.x == a) {
		if (this->a.center.x > this->b.center.x) {
			/*This is the new dot based on m.*/
			x = a - plus_x;
			y = b - plus_y;
		}
		else if (this->b.center.x > this->a.center.x) {
			x = a + plus_x;
			y = b + plus_y;
		}
	}
	else if (this->b.p1.x == a || this->b.p2.x == a) { // entra aqui quando � destino.
		if (this->a.center.x > this->b.center.x) { // e quando o destino est� � esquerda da origem
			/*This is the new dot based on m.*/
			x = a + plus_x;
			y = b + plus_y;
		}
		else {
			x = a - plus_x;
			y = b - plus_y;
		}
	}

}

void Road::compute_middle_dot_no_declive(GLfloat a, GLfloat b, GLfloat radius, GLfloat &x, GLfloat &y) {
	/*Checks Origin POI position.*/
	if (this->a.p1.x == a || this->a.p2.x == a) {
		if (this->a.center.x > this->b.center.x) {
			/*This is the new dot based on m.*/
			x = a - radius;
			y = b;
		}
		else if (this->b.center.x > this->a.center.x) {
			x = a + radius;
			y = b;
		}
	}
	else if (this->b.p1.x == a || this->b.p2.x == a) {
		if (this->a.center.x > this->b.center.x) {
			x = a + radius;
			y = b;
		}
		else {
			x = a - radius;
			y = b;
		}
	}
}

bool Road::checkInterceptionWithBase(GLfloat x, GLfloat y, GLfloat a, GLfloat b) {
	GLfloat value = (GLfloat)((pow((x - a), 2)) + (pow((y - b), 2)) - pow(RADIUS, 2));
	if (value >= 0) {
		return true;
	}
	return false;
}

BOOLEAN Road::is_the_point_inside(GLfloat x, GLfloat y) {
	Point ponto_a_testar, ponto_a_p1, ponto_a_p2, ponto_a_p3, ponto_a_p4,
		ponto_b_p1, ponto_b_p2, ponto_b_p3, ponto_b_p4, rampa[4], quadrado_a[4], quadrado_b[4];
	ponto_a_testar.x = x;
	ponto_a_testar.y = y;

	ponto_a_p1.x = this->a.p1.x;
	ponto_a_p1.y = this->a.p1.y;
	ponto_a_p2.x = this->a.p2.x;
	ponto_a_p2.y = this->a.p2.y;
	ponto_a_p3.x = this->a.p3.x;
	ponto_a_p3.y = this->a.p3.y;
	ponto_a_p4.x = this->a.p4.x;
	ponto_a_p4.y = this->a.p4.y;

	ponto_b_p1.x = this->b.p1.x;
	ponto_b_p1.y = this->b.p1.y;
	ponto_b_p2.x = this->b.p2.x;
	ponto_b_p2.y = this->b.p2.y;
	ponto_b_p3.x = this->b.p3.x;
	ponto_b_p3.y = this->b.p3.y;
	ponto_b_p4.x = this->b.p4.x;
	ponto_b_p4.y = this->b.p4.y;



	quadrado_a[0] = ponto_a_p1;
	quadrado_a[1] = ponto_a_p3;
	quadrado_a[2] = ponto_a_p4;
	quadrado_a[3] = ponto_a_p2;

	quadrado_b[0] = ponto_b_p1;
	quadrado_b[1] = ponto_b_p3;
	quadrado_b[2] = ponto_b_p4;
	quadrado_b[3] = ponto_b_p2;

	rampa[0] = ponto_a_p3;
	rampa[1] = ponto_a_p4;
	rampa[2] = ponto_b_p4;
	rampa[3] = ponto_b_p3;

	//Testa quadrado a
	int i, j, c = 0, nvert = 4;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((quadrado_a[i].y > ponto_a_testar.y) != (quadrado_a[j].y > ponto_a_testar.y)) &&
			(ponto_a_testar.x < (quadrado_a[j].x - quadrado_a[i].x) * (ponto_a_testar.y - quadrado_a[i].y) / (quadrado_a[j].y - quadrado_a[i].y) + quadrado_a[i].x))
			c = !c;
	}
	if (c != 0) {
		return c;
	}

	//Testa quadrado a
	c = 0, nvert = 4;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((quadrado_b[i].y > ponto_a_testar.y) != (quadrado_b[j].y > ponto_a_testar.y)) &&
			(ponto_a_testar.x < (quadrado_b[j].x - quadrado_b[i].x) * (ponto_a_testar.y - quadrado_b[i].y) / (quadrado_b[j].y - quadrado_b[i].y) + quadrado_b[i].x))
			c = !c;
	}
	if (c != 0) {
		return c;
	}

	//Testa Rampa
	c = 0, nvert = 4;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((rampa[i].y > ponto_a_testar.y) != (rampa[j].y > ponto_a_testar.y)) &&
			(ponto_a_testar.x < (rampa[j].x - rampa[i].x) * (ponto_a_testar.y - rampa[i].y) / (rampa[j].y - rampa[i].y) + rampa[i].x))
			c = !c;
	}

	return c;
}

GLfloat Road::is_the_point_inside_returnz(GLfloat x, GLfloat y) {
	Point ponto_a_testar, ponto_a_p1, ponto_a_p2, ponto_a_p3, ponto_a_p4,
		ponto_b_p1, ponto_b_p2, ponto_b_p3, ponto_b_p4, rampa[4], quadrado_a[4], quadrado_b[4];
	ponto_a_testar.x = x;
	ponto_a_testar.y = y;

	ponto_a_p1.x = this->a.p1.x;
	ponto_a_p1.y = this->a.p1.y;
	ponto_a_p2.x = this->a.p2.x;
	ponto_a_p2.y = this->a.p2.y;
	ponto_a_p3.x = this->a.p3.x;
	ponto_a_p3.y = this->a.p3.y;
	ponto_a_p4.x = this->a.p4.x;
	ponto_a_p4.y = this->a.p4.y;

	ponto_b_p1.x = this->b.p1.x;
	ponto_b_p1.y = this->b.p1.y;
	ponto_b_p2.x = this->b.p2.x;
	ponto_b_p2.y = this->b.p2.y;
	ponto_b_p3.x = this->b.p3.x;
	ponto_b_p3.y = this->b.p3.y;
	ponto_b_p4.x = this->b.p4.x;
	ponto_b_p4.y = this->b.p4.y;

	quadrado_a[0] = ponto_a_p1;
	quadrado_a[1] = ponto_a_p3;
	quadrado_a[2] = ponto_a_p4;
	quadrado_a[3] = ponto_a_p2;

	quadrado_b[0] = ponto_b_p1;
	quadrado_b[1] = ponto_b_p3;
	quadrado_b[2] = ponto_b_p4;
	quadrado_b[3] = ponto_b_p2;

	rampa[0] = ponto_a_p3;
	rampa[1] = ponto_a_p4;
	rampa[2] = ponto_b_p4;
	rampa[3] = ponto_b_p3;

	//Testa quadrado a
	int i, j, c = 0, nvert = 4;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((quadrado_a[i].y > ponto_a_testar.y) != (quadrado_a[j].y > ponto_a_testar.y)) &&
			(ponto_a_testar.x < (quadrado_a[j].x - quadrado_a[i].x) * (ponto_a_testar.y - quadrado_a[i].y) / (quadrado_a[j].y - quadrado_a[i].y) + quadrado_a[i].x))
			c = !c;
	}
	if (c != 0) {
		return this->a.center.z;
	}

	//Testa quadrado a
	c = 0, nvert = 4;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((quadrado_b[i].y > ponto_a_testar.y) != (quadrado_b[j].y > ponto_a_testar.y)) &&
			(ponto_a_testar.x < (quadrado_b[j].x - quadrado_b[i].x) * (ponto_a_testar.y - quadrado_b[i].y) / (quadrado_b[j].y - quadrado_b[i].y) + quadrado_b[i].x))
			c = !c;
	}
	if (c != 0) {
		return this->b.center.z;
	}

	//Testa Rampa
	c = 0, nvert = 4;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((rampa[i].y > ponto_a_testar.y) != (rampa[j].y > ponto_a_testar.y)) &&
			(ponto_a_testar.x < (rampa[j].x - rampa[i].x) * (ponto_a_testar.y - rampa[i].y) / (rampa[j].y - rampa[i].y) + rampa[i].x))
			c = !c;
	}

	if (c != 0) {
		if (a.center.z == b.center.z) {
			return a.center.z;
		}
		else {
			float distance_first, distance_second, distance_total, tempx, tempy;
			tempx = (GLfloat)(a.p3.x - x);
			tempx = (GLfloat)pow(tempx, 2.0f);
			tempy = (GLfloat)(a.p3.y - y);
			tempy = (GLfloat)pow(tempy, 2.0f);
			distance_first = (GLfloat)tempx + tempy;
			distance_first = (GLfloat)sqrt(distance_first);
			tempx = (GLfloat)(b.p4.x - x);
			tempx = (GLfloat)pow(tempx, 2.0f);
			tempy = (GLfloat)(b.p4.y - y);
			tempy = (GLfloat)pow(tempy, 2.0f);
			distance_second = (GLfloat)tempx + tempy;
			distance_second = (GLfloat)sqrt(distance_second);
			distance_total = (GLfloat)distance_first + distance_second;
			float percentage = (GLfloat)(distance_first / distance_total);
			GLfloat difference_z = (GLfloat)b.center.z - a.center.z;
			return (GLfloat)percentage * difference_z + a.center.z;
		}
	}
	return -1;
}

int Road::getOrigemID() {
	return this->poi_origem_id;
}

int Road::getDestinoID() {
	return this->poi_destino_id;
}

bool Road::addRoad(Road road) {
	vector<Road>::iterator it;
	for (it = Road_container.begin(); it != Road_container.end(); it++) {
		if ((it->getOrigemID() == road.getDestinoID() && it->getOrigemID() == road.getOrigemID()) ||
			it->getDestinoID() == road.getOrigemID() && it->getOrigemID() == road.getDestinoID()) {
			return false;
		}
	}
	Road_container.push_back(road);
	return true;
}
