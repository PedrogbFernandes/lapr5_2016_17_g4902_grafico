#include "Weather.h"

#define MAX_PARTICLES 1000
#define RAIN	0
#define SNOW	1
#define	HAIL	2


float slowdown = 2.0;
float velocity = 0.0;
float pan = 0.0;
float tilt = 0.0;
float hailsize = 0.1;

int loop;
int fall;

//floor colors
float r = 0.0;
float g = 1.0;
float b = 0.0;
float ground_points[21][21][3];
float ground_colors[21][21][4];
float accum = -10.0;

GLint snowball;

typedef struct {
	// Life
	bool alive;	// is the particle alive?
	float life;	// particle lifespan
	float fade; // decay
				// color
	float red;
	float green;
	float blue;
	// Position/direction
	float xpos;
	float ypos;
	float zpos;
	// Velocity/Direction, only goes down in y dir
	float vel;
	// Gravity
	float gravity;
}particles;

// Paticle System
particles par_sys[MAX_PARTICLES];



// Initialize/Reset Particles - give them their attributes
void Weather::initParticles(int i, GLfloat character_x, GLfloat character_y, GLfloat character_z) {
	par_sys[i].alive = true;
	par_sys[i].life = 5.0;
	par_sys[i].fade = float(rand() % 1000) / 1000.0f + 0.003f;

	par_sys[i].xpos = (float)(rand() % 81) - 40 + character_x;
	par_sys[i].ypos = (float)(rand() % 81) - 40 + character_y;
	par_sys[i].zpos = character_z + 40;

	par_sys[i].red = 0.5;
	par_sys[i].green = 0.5;
	par_sys[i].blue = 1.0;

	par_sys[i].vel = velocity;
	par_sys[i].gravity = -0.8;//-0.8;

}


void Weather::drawRain(GLfloat character_x, GLfloat character_y, GLfloat character_z) {

	float x, y, z;
	for (loop = 0; loop < MAX_PARTICLES; loop = loop + 2) {
		if (par_sys[loop].alive == true) {
			x = par_sys[loop].xpos;
			y = par_sys[loop].ypos;
			z = par_sys[loop].zpos;

			// Draw particles
			glColor3f(0.5, 0.5, 1.0);
			glBegin(GL_LINES);
			glVertex3f(x, y, z);
			glVertex3f(x, y, z + 0.5);
			glEnd();

			// Update values
			//Move
			// Adjust slowdown for speed!
			par_sys[loop].zpos += par_sys[loop].vel / (slowdown * 1000);
			par_sys[loop].vel += par_sys[loop].gravity;
			// Decay
			par_sys[loop].life -= par_sys[loop].fade;

			if (par_sys[loop].zpos <= -10) {
				par_sys[loop].life = -1.0;
			}
			//Revive 
			if (par_sys[loop].life < 0.0) {
				initParticles(loop,character_x,character_y,character_z);
			}
			glColor4f(1.0, 1.0, 1.0, 1.0);
		}
	}
}

void Weather::drawSnow(GLfloat character_x, GLfloat character_y, GLfloat character_z) {
	float x, y, z;
	for (loop = 0; loop < MAX_PARTICLES; loop = loop + 2) {
		if (par_sys[loop].alive == true) {
			x = par_sys[loop].xpos;
			y = par_sys[loop].ypos;
			z = par_sys[loop].zpos;

			// Draw particles
			glColor3f(1.0, 1.0, 1.0);
			glPushMatrix();
			glTranslatef(x, y, z);
			glCallList(snowball);
			glPopMatrix();

			// Update values
			//Move
			par_sys[loop].zpos += par_sys[loop].vel / (slowdown * 1000);
			par_sys[loop].vel += par_sys[loop].gravity;
			// Decay
			par_sys[loop].life -= par_sys[loop].fade;

			if (par_sys[loop].zpos <= -10) {
				/*int yi = y + 10;
				int xi = x + 10;
				ground_colors[yi][xi][0] = 1.0;
				ground_colors[yi][xi][2] = 1.0;
				ground_colors[yi][xi][3] += 1.0;
				if (ground_colors[yi][xi][3] > 1.0) {
					ground_points[xi][yi][1] += 0.1;
				}*/
				par_sys[loop].life = -1.0;
			}

			//Revive 
			if (par_sys[loop].life < 0.0) {
				initParticles(loop, character_x, character_y, character_z);
			}
			glColor4f(1.0, 1.0, 1.0, 1.0);
		}
	}
}


void Weather::init(GLfloat character_x, GLfloat character_y, GLfloat character_z) {

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearDepth(1.0);
	glEnable(GL_DEPTH_TEST);

	snowball = glGenLists(1);
	glNewList(snowball, GL_COMPILE);
	glutSolidSphere(0.2, 16, 16);
	glPushMatrix();

	glPopMatrix();
	glEndList();

	// Initialize particles
	for (loop = 0; loop < MAX_PARTICLES; loop++) {
		initParticles(loop, character_x, character_y, character_z);
	}
}