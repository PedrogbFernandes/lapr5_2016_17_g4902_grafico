#pragma once

#include "Resources.h"
#include "POI.h"
#include "Road.h"
/*This class is aware of the Character position as well as all POI position.*/
class Collider
{

	static GLfloat inc_value;
	static GLfloat angle_ref;
	static vector<GLfloat> y_closer;
	static vector<GLfloat> x_closer;
	static GLfloat wanted_y;
	static GLfloat wanted_x;
	static int count;
	static int quadrante;
private:
	/*computes the slope and the y-interception.*/
	void line_equation(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat &m, GLfloat &b);
public:
	Collider();
	//Collider(Character persona, vector<POI> pois);
	~Collider();
	string checkCharacterPositionRelativeToPOI(GLfloat *persona_vector, vector<POI> pois_list);
	GLfloat checkCharacterPositionRelativeToPOIAndReturnsZ(GLfloat character_x, GLfloat character_y, GLfloat character_z, vector<POI> pois_list, vector<Road> roads_list);
	BOOLEAN checkPostionIsInsideGrid(GLfloat c_x, GLfloat c_y, vector<POI> pois_list, vector<Road> roads_list);
	/*Computes the new Character Position.*/
	void checkNextLinePoint(GLfloat x, GLfloat y, POI &p2, GLfloat &newx, GLfloat &newy);
	void computeNextPositionBasedonCircus(GLfloat previous_x, GLfloat previous_y, POI &p, POI &previous_p, GLfloat radius, GLfloat &new_x, GLfloat &new_y);
	bool checkCharacterPositonInsideRadius(GLfloat previous_x, GLfloat previous_y, POI &p, GLfloat radius);
	bool ckeckCharacterBelongsToNewLine(GLfloat char_x, GLfloat char_y, POI &p, POI &next_p);
};

