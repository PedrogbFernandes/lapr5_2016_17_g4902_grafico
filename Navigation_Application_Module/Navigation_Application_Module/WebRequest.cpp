#include "WebRequest.h"

using namespace web::json;                  // JSON library
using namespace utility;                    // Common utilities like string conversions
using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams

void WebRequest::PoisHTTPRequest(const char* filename) {

	deleteFile(filename);

	auto fileStream = std::make_shared<ostream>();

	// Open stream to output file.
	pplx::task<void> requestTask = fstream::open_ostream(U("pois_response.json")).then([=](ostream outFile)
	{
		*fileStream = outFile;

		//Create Client.
		http_client client(U("http://10.8.11.86"));
		//http_client *client = new http_client(L"http://10.8.11.86");

		//Build request URI.
		uri_builder builder(U("/BDAPI/api/POI"));
		pplx::task<http_response> response = client.request(methods::GET, builder.to_string());
		return response;
	})

		// Handle response headers arriving.
		.then([=](http_response response)
	{
		printf("Received response status code:%u\n", response.status_code());

		// Write response body into the file.
		return response.body().read_to_end(fileStream->streambuf());
	})

		// Close the file stream.
		.then([=](size_t)
	{
		return fileStream->close();
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
}

void WebRequest::LoginHttpRequest(const char* filename, char* username, char* password) {
	/*Setting up the required json data.*/
	/*json::value postData;
	postData[L"grant-type"] = json::value::string(L"password");
	postData[L"username"] = json::value::string(L"gestor@mail.com");
	postData[L"password"] = json::value::string(L"!Password1");*/
	deleteFile(filename);

	char* till_username = "grant_type=password&username=";
	char* till_password = "&password=";
	const size_t username_size = strlen(username) + 1;
	const size_t password_size = strlen(password) + 1;
	const size_t tillusername_size = strlen(till_username) + 1;
	const size_t tillpassword_size = strlen(till_password) + 1;
	//client.request(methods::POST, L"", L"grant_type=password&username=gestor@mail.com&password=!Password1", L"application/x-www-form-urlencoded");

	/*std::wstring username_(username_size, L'#');
	mbstowcs(&username_[0], username, username_size);
	std::wstring password_(password_size, L'#');
	mbstowcs(&password_[0], password, password_size);
	std::wstring tillusername_(tillusername_size, L'#');
	mbstowcs(&tillusername_[0], till_username, tillusername_size);
	std::wstring tillpassword_(tillpassword_size, L'#');
	mbstowcs(&tillpassword_[0], till_password, tillpassword_size);*/

	/*Retrieving pointers size.*/
	const size_t info_size = strlen(username) + strlen(password) + strlen(till_username) + strlen(till_password) + 1;

	/*Allocating memory.*/
	char* info_not_rooted = (char*)malloc(info_size + 1);

	/*Appending char pointers.*/
	sprintf(info_not_rooted, "%s%s%s%s", till_username, username, till_password, password);

	/*Creating wstring value.*/
	std::wstring info(info_size, L'#');
	mbstowcs(&info[0], info_not_rooted, info_size);

	auto fileStream = std::make_shared<ostream>();

	// Open stream to output file.
	pplx::task<void> requestTask = fstream::open_ostream(U("login_response.json")).then([=](ostream outFile)
	{
		*fileStream = outFile;
		http_client client(L"http://10.8.11.86/BDAPI/Token");
		return client.request(methods::POST, L"", info, L"application/x-www-form-urlencoded");
	})

		// Handle response headers arriving.
		.then([=](http_response response)
	{
		printf("Received response status code:%u\n", response.status_code());

		// Write response body into the file.

		return response.body().read_to_end(fileStream->streambuf());
	})

		// Close the file stream.
		.then([=](size_t)
	{
		return fileStream->close();
		free(info_not_rooted);
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
}

void WebRequest::PathHttpRequest(const char* filename, char* token, char *username) {
	deleteFile(filename);

	/*Setting up the Header value.*/
	char* bearer = "Bearer ";
	const size_t header_value_size = strlen(bearer) + strlen(token) + 1;
	char* value = (char*)malloc(header_value_size);
	sprintf(value, "%s%s", bearer, token);

	auto fileStream = std::make_shared<ostream>();

	// Open stream to output file.
	pplx::task<void> requestTask = fstream::open_ostream(U("path_response.json")).then([=](ostream outFile)
	{
		*fileStream = outFile;
		http_client client(L"http://10.8.11.86/BDAPI/api/Percurso");
		http_request request(methods::GET);
		request.headers().add(L"Authorization", value);
		return client.request(request);
	})
		// Handle response headers arriving.
		.then([=](http_response response)
	{
		printf("Received response status code:%u\n", response.status_code());

		// Write response body into the file.

		return response.body().read_to_end(fileStream->streambuf());
	})

		// Close the file stream.
		.then([=](size_t)
	{
		return fileStream->close();
	});

	// Wait for all the outstanding I/O to complete and handle any exceptions
	try
	{
		requestTask.wait();
	}
	catch (const std::exception &e)
	{
		printf("Error exception:%s\n", e.what());
	}
}

bool WebRequest::deleteFile(const char * filename) {
	if (remove(filename) != 0) {
		return false;
	}
	else {
		return true;
	}
}

