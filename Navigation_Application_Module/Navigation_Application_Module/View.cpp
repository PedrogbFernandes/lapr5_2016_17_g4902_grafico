#include "View.h"

View::View() {
}

View::View(GLdouble fov, int width, int height, GLdouble closerPerspectivePoint, GLdouble furtherPerspectivePoint) {
	this->fov = fov;
	this->width = width;
	this->height = height;
	this->closerPerspectivePoint = closerPerspectivePoint;
	this->furtherPerspectivePoint = furtherPerspectivePoint;
	this->sidebar_width =(GLfloat)width;
}

View::~View() {
}

void View::setPerspectiveView(GLdouble fov, GLdouble closerPerspectivePoint, GLdouble furtherPerspectivePoint) {
	/*Sets up a perspective tridimensional View.*/
	gluPerspective(fov, (GLdouble)this->width / this->height, closerPerspectivePoint, furtherPerspectivePoint);
}

void View::setOrthogonalView(GLdouble fov, GLdouble closerPoint, GLdouble furtherPoint) {
	/*Sets up an Orthogonal tridimensional View.*/
	if (this->width < this->height) {
		glOrtho(-fov, fov, -fov * (GLdouble)this->height / this->width, fov* (GLdouble)this->height / this->width, closerPoint, furtherPoint);
	}
	else {
		glOrtho(-fov * (GLdouble)this->width / this->height, fov* (GLdouble)this->width / this->height, -fov, fov, closerPoint, furtherPoint);
	}
}
/*Sets up an Orthogonal 2D View.*/
void View::setOrthogonal2DView() {
	gluOrtho2D(0.0, this->width, 0.0, this->height);
}

void View::incrementSidebarWidth(GLfloat inc) {

	GLfloat inf_limiter = (GLfloat) WIDTH_ON_TEXT(this->width); //limite_inferior.
	GLfloat sup_limiter = (GLfloat) this->width;

	this->sidebar_width += inc;

	if (this->sidebar_width <= inf_limiter) {
		this->sidebar_width = inf_limiter;
	}
	else if (this->sidebar_width >= sup_limiter) {
		this->sidebar_width = sup_limiter;
	}
}

void View::drawSidebar() {
	//GLfloat width = (GLfloat)this->width;
	//GLfloat height = (GLfloat)this->height;
	//glBegin(GL_QUADS);
	//glColor3f(0.234f, 0.234f, 0.254f);
	//glVertex2f(this->sidebar_width, height);
	//glVertex2f(this->sidebar_width, 0.0f);
	//glVertex2f(width, 0.0f);
	//glVertex2f(width, height);
	//glEnd();

	//glBegin(GL_QUADS);
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glVertex2f(this->sidebar_width + 50.0f, this->height - 100.0f);
	//glVertex2f(this->sidebar_width + 50.0f, 100.0f);
	//glVertex2f(width, 100.0f);
	//glVertex2f(width,height - 100.0f);
	//glEnd();

	///*Contornos.*/
	//glBegin(GL_LINE_LOOP);
	//glColor3f(0.0f, 0.0f, 0.0f);
	//glVertex2f(this->sidebar_width, this->height);
	//glVertex2f(this->sidebar_width, 0.0f);
	//glVertex2f(this->sidebar_width, 0.0f);
	//glVertex2f(this->width, 0.0f);
	//glVertex2f(this->width, this->height);
	//glEnd();

	//glBegin(GL_LINE_LOOP);
	//glColor3f(0.0f, 0.0f, 0.0f);
	//glVertex2f(this->sidebar_width + 50.0f, this->height - 100.0f);
	//glVertex2f(this->sidebar_width + 50.0f, 100.0f);
	//glVertex2f(this->width, 100.0f);
	//glVertex2f(this->width, this->height - 100.0f);
	//glEnd();
	//
	//
}

/*This methods sets up the side bar using the same viewport for both perspective and orthogonal view.*/
void View::setSidebar(GLfloat inc) {
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0.0, this->width, 0.0, this->height);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
	incrementSidebarWidth(inc);
	drawSidebar();
	glPopMatrix();
	glPopMatrix();
}

void View::setViewport(int x, int y, int width, int height) {
	this->width = width;
	this->sidebar_width = (GLfloat)width;
	this->height = height;
	glViewport(0, 0,(GLint)this->width, (GLint)this->height);
}

void View::size(int &width, int &height) {
	width = this->width;
	height = this->height;
}
