#include "TextureFactory.h"

Texture* TextureFactory::loadTexture(const char * filename) {
	GLuint textureID;
	/*Loading texture as it is on the disk, creating a new ID and enabling mipmapping.*/
	textureID = SOIL_load_OGL_texture(filename,
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_INVERT_Y);

	glEnable(GL_TEXTURE_2D);
	/*Binds texture by its ID.*/
	glBindTexture(GL_TEXTURE_2D, textureID);
	/*Each texture has an ID*/
	Texture * texture = new Texture(filename, textureID);
	/*Unbind texture.*/
	glBindTexture(GL_TEXTURE_2D, 0);
	/*Disabling behaviour*/
	glDisable(GL_TEXTURE_2D);	
	return texture;
}
