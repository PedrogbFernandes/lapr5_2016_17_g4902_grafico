#include "VAOFactory.h"

/*Defining Static Vectors.*/
vector<GLuint> VAOFactory::VAOs;
vector<GLuint> VAOFactory::VBOs;
vector<GLuint> VAOFactory::textures;

int VAOFactory::createVAO() {
	GLuint vaoID;
	try {
		/*Generates one single VAO.*/
		glGenVertexArrays(1, &vaoID);
	}
	catch (exception exp) {
		//handling exception.
	}
	/*saving current VAO.*/
	VAOs.push_back(vaoID);
	/*Binds the VAO by its id.*/
	glBindVertexArray(vaoID);
	return vaoID;
}

/*Binds to VAO using the Float Vertex Array that contains the data to be rendered.*/
void VAOFactory::bindVBOintoAttributeList(int index, vector<GLfloat> data, bool texture) {
	int vertex_size = VERTEX_SIZE;
	/*For texture only.*/
	if (texture == true) {
		vertex_size = TEXTURE_SIZE_VERTEX;
	}
	GLuint vboID;
	/*Generates one single VBO.*/
	glGenBuffers(1, &vboID);
	/*Saving current VBO.*/
	VBOs.push_back(vboID);
	/*Specifying the VBO type. */
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	/*Generating the buffer as a pointer to the data.*/
	float *buffer = allocateFloatBuffer(data);
	int size = data.size();
	/*Storing data into the VBO while never editing again.*/
	glBufferData(GL_ARRAY_BUFFER, size * sizeof(GLfloat), buffer, GL_STATIC_DRAW);
	/*This is where the VBO is assigned to the VAO in a specific index.*/
	glVertexAttribPointer(index, vertex_size, GL_FLOAT, GL_FALSE, 0, 0);
	/*Unbind VBO.*/
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	free(buffer);
}
 
/*Binds to VAO using the int index position of each vertex to render later.*/
void VAOFactory::bindVBOIndexBuffer(vector<GLuint> data, bool texture) {
	/*Element type VBO.*/
	GLuint indexbufferID; 
	glGenBuffers(1, &indexbufferID);
	VBOs.push_back(indexbufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbufferID);
	GLuint *buffer = allocateIntBuffer(data);
	int size = data.size();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size * sizeof(GLuint), buffer, GL_STATIC_DRAW);
	/*This is not a vertex attribute pointer on the VAO slot.*/
	free(buffer);
}

void VAOFactory::unbindVAO() {
	/*Unbinds previous VAO.*/
	glBindVertexArray(0);
}

/*GLfloat buffer*/
float* VAOFactory::allocateFloatBuffer(vector<GLfloat> data) {
	/*Pointer to the allocated memory.*/
	//GLfloat *buffer = &data[0];
	int size = data.size();
	GLfloat *buffer;
	//buffer = (GLfloat*)malloc(size * sizeof(GLfloat));
	buffer = (GLfloat*)malloc(size * sizeof(GLfloat));
	for (int i = 0; i < size; i++) {
		*(buffer + i) = data.at(i); //converting the data.
	}
	return buffer;
}
	
/*For index buffer - GLuint Buffer.*/
GLuint* VAOFactory::allocateIntBuffer(vector<GLuint> data) {
	GLuint size = data.size();
	GLuint *buffer;
	buffer = (GLuint*)malloc(size * sizeof(GLuint));
	for (size_t i = 0; i < size; i++) {
		*(buffer + i) = data.at(i);
	}
	return buffer;
}

GLuint VAOFactory::loadTexture(const char * filename){
	Texture *texture_id = TextureFactory::loadTexture(filename);
	return texture_id->getTextureID();
}

VAO* VAOFactory::loadDatatoVAO(const char *texture,  vector<GLfloat> vertex_data, vector<GLfloat> texture_data, vector <GLfloat> normal_data, vector<GLuint> index_data) {
	GLuint texture_id = loadTexture(texture);
	GLuint vaoID = createVAO();
	//bindVBOIndexBuffer(index_data, false); // This operation requires the index_data but is not being used atm.
	/* Stores at a specific att list.*/
	bindVBOintoAttributeList(0, vertex_data, false);
	bindVBOintoAttributeList(1, texture_data, true);
	//bindVBOintoAttributeList(2, normal_data, false);
	/*required operation.*/
	unbindVAO();
	int n_vertex = (vertex_data.size() / 3);
	VAO *vao = new VAO(vaoID, n_vertex, texture_id);
	//VAO *vao = new VAO(vaoID, index_data.size(), texture_id); // To use with index_data.
	return vao;
}

/*Freeing all used memory.*/
void VAOFactory::deleteData() {
	//vector<GLuint> ::iterator it;
	int size = VAOs.size();
	GLuint *p = &VAOs[0];
	glDeleteVertexArrays(size, p);

	int size_ = VBOs.size();
	GLuint *p2 = &VBOs[0];
	glDeleteBuffers(size_, p2);

	int size_2 = textures.size();
	GLuint *p3 = &textures[0];
	glDeleteTextures(size_2,p3);
}