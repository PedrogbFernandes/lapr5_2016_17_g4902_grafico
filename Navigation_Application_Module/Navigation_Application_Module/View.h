#pragma once

#include "Resources.h"
class View
{

private:

	/*TODO: won't need this.*/
	GLdouble fov;
	int width;
	int height;
	GLdouble closerPerspectivePoint;
	GLdouble furtherPerspectivePoint;

	GLfloat sidebar_width;

	void incrementSidebarWidth(GLfloat inc);
	void drawSidebar();

public:
	View();
	View(GLdouble fov, int width, int height,
		GLdouble closerPerspectivePoint, GLdouble furtherPerspectivePoint);
	~View();

	void setPerspectiveView(GLdouble fov, GLdouble closerPerspectivePoint, GLdouble furtherPerspectivePoint);
	void setOrthogonalView(GLdouble fov, GLdouble closerPoint, GLdouble furtherPoint);
	void setOrthogonal2DView();

	void setSidebar(GLfloat inc);

	void setViewport(int x, int y, int width, int height);
	void size(int &width, int &height);
};

