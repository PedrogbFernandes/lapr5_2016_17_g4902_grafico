#pragma once
#include <stdexcept>
#include "Resources.h"
#include "POI.h"
#include "Road.h"
#include "User.h"
#include "Path.h"

static string const ID = "ID";
static string const Name = "Name";
static string const Description = "Description";
static string const GPS_Lat = "GPS_Lat";
static string const GPS_Long = "GPS_Long";
static string const Altitude = "Altitude";
static string const Creator = "Creator";
static string const Approved = "Approved";
static string const ConnectedPOI = "ConnectedPOI";
static string const Hashtags = "Hashtags";
static string const OpenHour = "OpenHour";
static string const CloseHour = "CloseHour";
static string const Text = "Text";

static string const Token = "access_token";
static string const UserName = "userName";
static string const Bearer = "bearer";

static string const PercursoPOIsOrder = "PercursoPOIsOrder";

class JSONParser
{
private:

	/*Parse POI File.*/
	static vector<string> poi;
	static vector<vector<string>> pois;
	static string poi_connected_ids;
	static string poi_hashtags;
	static bool handleConnections;
	static bool handleHashTags;
	static int checkReference2(int c, string ref, string &line);
	static int checkReference3(int ret, string &line);
	static void checkReference(string &line);
	static int handlePOIconnections(int ret, string &line);
	static int handleHashtags(int ret, string &line);
	static vector<POI> generatePOIS();
	static void fillReferences();
	static vector<POI> handlePOIstring(vector<vector<int>> &references, string &line);

	/*Parse Login File.*/
	static int checkLoginRequestTokens(string &line, int c, string ref);
	static string extractLoginInfo(string &line, int &c);
	static bool deletefile(const char* filename);

	/*Parse Login File.*/
	static string checkReferencePath(int &i, string &line, bool is_it_a_path);
	static vector<int> extractPathIDs(string &path_ids);

public:
	static vector<POI> parseFile(const char* filename);
	static User* parseLoginfile(const char*filename);
	static vector<Path> parsePathFile(const char* filename);
};

