#pragma once

#include "Resources.h"

class Weather
{

private:


public:

	void initParticles(int i, GLfloat character_x, GLfloat character_y, GLfloat character_z);

	void drawRain(GLfloat character_x, GLfloat character_y, GLfloat character_z);

	void drawSnow(GLfloat character_x, GLfloat character_y, GLfloat character_z);

	void init(GLfloat character_x, GLfloat character_y, GLfloat character_z);

};