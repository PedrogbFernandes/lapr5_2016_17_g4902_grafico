#pragma once
class Texture
{

private:
	const char *filename;
	int textureID;

public:
	Texture(const char *filename, int textureID);
	~Texture();

	char* getFilename();
	int getTextureID();
};

