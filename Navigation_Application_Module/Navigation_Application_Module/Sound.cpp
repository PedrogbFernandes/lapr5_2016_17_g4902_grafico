#include "Sound.h"
typedef struct {// OpenAL Struct
	ALuint	buffer, source;
} EstadoAL;

float volume;

ALint state_main;
ALint state_poi;
ALint state_step;

int mute;

EstadoAL main_music;
EstadoAL poi_music;
EstadoAL step_music;

void Sound::init() {

	mute = 0;
	volume = 0.5;

	main_music.buffer = alutCreateBufferFromFile("res/sound/main.wav");
	alGenSources(1, &main_music.source);
	alSourcei(main_music.source, AL_LOOPING, 1);
	alSourcef(main_music.source, AL_GAIN, volume);
	alSourcei(main_music.source, AL_BUFFER, main_music.buffer);

	poi_music.buffer = alutCreateBufferFromFile("res/sound/poi.wav");
	alGenSources(1, &poi_music.source);
	alSourcef(poi_music.source, AL_GAIN, 1.0);
	alSourcei(poi_music.source, AL_BUFFER, poi_music.buffer);

	step_music.buffer = alutCreateBufferFromFile("res/sound/step.wav");
	alGenSources(1, &step_music.source);
	alSourcei(step_music.source, AL_LOOPING, 1);
	alSourcef(step_music.source, AL_GAIN, 0.2);
	alSourcei(step_music.source, AL_BUFFER, step_music.buffer);

	alGetSourcei(main_music.source, AL_SOURCE_STATE, &state_main);
	alGetSourcei(poi_music.source, AL_SOURCE_STATE, &state_poi);
	alGetSourcei(step_music.source, AL_SOURCE_STATE, &state_step);
}

void Sound::ChangeVolume() {
	if (mute == 0) {
		alSourcef(main_music.source, AL_GAIN, 0);
		alSourcef(poi_music.source, AL_GAIN, 0);
		alSourcef(step_music.source, AL_GAIN, 0);
		mute = 1;
	}
	else {
		alSourcef(main_music.source, AL_GAIN, 0.5);
		alSourcef(poi_music.source, AL_GAIN, 1.0);
		alSourcef(step_music.source, AL_GAIN, 0.2);
		mute = 0;
	}
}

void Sound::Volume_Down() {
	if (volume - 0.1 > 0) {
		volume -= 0.1;
		ChangeVolume();
	}
}

void Sound::Volume_Up() {
	if (volume + 0.1 < 1) {
		volume += 0.1;
		ChangeVolume();
	}
}

void Sound::PlayMain() {
	alSourcePlay(main_music.source);
}

void Sound::PlayPoi() {
	alSourcePlay(poi_music.source);
}

void Sound::PlayStep() {
	alSourcePlay(step_music.source);
}

void Sound::StopStep() {
	alSourceStop(step_music.source);
}