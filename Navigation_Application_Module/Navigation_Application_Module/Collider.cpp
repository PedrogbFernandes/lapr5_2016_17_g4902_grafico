#include "Collider.h"

GLfloat Collider::inc_value = 0.25f;
GLfloat Collider::angle_ref = 1.0f;
GLfloat Collider::wanted_y;
GLfloat Collider::wanted_x;
/*possible solutions.*/
vector<GLfloat> Collider::y_closer;
vector<GLfloat> Collider::x_closer;
int Collider::count = 0;
int Collider::quadrante = 0;

Collider::Collider() {
}

Collider::~Collider() {
}

string Collider::checkCharacterPositionRelativeToPOI(GLfloat *persona_position, vector<POI> pois_list) {
	GLfloat persona_x, persona_y, persona_z, poi_x, poi_y, poi_z, poi_radius;

	persona_x = *(persona_position + 0);
	persona_y = *(persona_position + 1);
	persona_z = *(persona_position + 2);

	vector<POI> ::iterator it;

	for (it = pois_list.begin(); it != pois_list.end(); it++) {

		poi_x = it->getX();
		poi_y = it->getY();
		poi_z = it->getZ();
		poi_radius = it->getRadius();

		GLfloat value = (GLfloat)((pow((persona_x - poi_x), 2)) + (pow((persona_y - poi_y), 2)) - pow(poi_cube_size, 2));
		if (value < 0) {
			return it->getName();
		}
	}
	return "";
}

void Collider::checkNextLinePoint(GLfloat x, GLfloat y, POI& p, GLfloat &newx, GLfloat &newy) {

	GLfloat m, b;
	GLfloat inc = 0.0f;

	if (y < p.getY()) {
		inc = 0.1f;
	}
	else if (y > p.getY()) {
		inc = -0.1f;
	}

	if (y == p.getY()) {
		if (x < p.getX()) {
			inc = 0.1f;
		}
		else if (x > p.getX()) {
			inc = -0.1f;
		}
	}

	if (inc == 0.0f) {
		newx = x;
		newy = y;
	}
	else {
		line_equation(x, y, p.getX(), p.getY(), m, b);
		//next x & y;
		newy = y + inc;
		newx = (newy - (b)) / m;
	}
}

void Collider::computeNextPositionBasedonCircus(GLfloat previous_x, GLfloat previous_y, POI & p, POI &previous_p, GLfloat radius, GLfloat & new_x, GLfloat & new_y) {
	// (x - a)^2 + (y - b)^2 - r^2 = 0;

	/*Controlo dos Quadrantes.*/
	GLfloat radius_aux_for_x, radius_aux_for_y;
	if (roundf(p.getX()) < roundf(previous_p.getX()) && roundf(p.getY()) < roundf(previous_p.getY())) {
		radius_aux_for_x = radius;
		radius_aux_for_y = radius;
	}
	else if (roundf(p.getX()) > roundf(previous_p.getX()) && roundf(p.getY()) < roundf(previous_p.getY())) {
		radius_aux_for_x = radius * -1.0f;
		radius_aux_for_y = radius;
	}
	else if (roundf(p.getX()) > roundf(previous_p.getX()) && roundf(p.getY()) > roundf(previous_p.getY())) {

		radius_aux_for_x = radius * -1.0f;
		radius_aux_for_y = radius * -1.0f;
	}
	else if (roundf(p.getX()) < roundf(previous_p.getX()) && roundf(p.getY()) > roundf(previous_p.getY())) {

		radius_aux_for_x = radius;
		radius_aux_for_y = radius * -1.0f;
	}
	else {
		radius_aux_for_x = radius;
		radius_aux_for_y = radius;
	}

	GLfloat cateto_oposto, cateto_adjacente;
	GLfloat ca = previous_x - p.getX();
	GLfloat co = previous_y - p.getY();

	GLfloat angle = degree(acos(ca / radius_aux_for_x)); //positivo
	GLfloat angle2 = degree(asin(co / radius_aux_for_y)); //positivo
	GLfloat angle3 = degree(atan(co / ca)); //negativo

	if (round(angle) == round(179.0f)) {
		angle_ref = -1.0f;
	}
	if (round(angle) == round(0)) {
		angle_ref = 1.0f;
	}
	angle = angle * angle_ref + inc_value;
	GLfloat x, y;
	x = radius_aux_for_x * cos(rad(angle));
	y = radius_aux_for_y * sin(rad(angle));

	new_x = x + p.getX();
	new_y = y + p.getY();

	/*GLfloat x = previous_x + inc;
	GLfloat y, y2;

	GLfloat c1 = (GLfloat)(pow(x - p.getX(), 2) - pow(radius, 2));
	GLfloat c2 = (GLfloat)(pow(p.getY(), 2));
	GLfloat c = c1 + c2;

	GLfloat b = (GLfloat)(2 * p.getY() * -1);
	GLfloat a = 1;

	GLfloat delta = pow(b, 2) - 4.0f * a * c;
	if (delta < 0) {
		cout << "Error on 2º degree Collider" << endl;
	}
	else {
		y = ((-b) + sqrt(delta)) / (2 * a);
		y2 = ((-b) - sqrt(delta)) / (2 * a);
	}*/
}

bool Collider::checkCharacterPositonInsideRadius(GLfloat previous_x, GLfloat previous_y, POI &p, GLfloat radius) {
	GLfloat value = (GLfloat)((pow((previous_x - p.getX()), 2)) + (pow((previous_y - p.getY()), 2)) - pow(radius, 2));
	if (value <= 0.01) {
		return true;
	}
	return false;
}

bool Collider::ckeckCharacterBelongsToNewLine(GLfloat char_x, GLfloat char_y, POI & p, POI & next_p) {
	if (roundf(next_p.getX()) < roundf(p.getX()) && roundf(next_p.getY()) < roundf(p.getY())) {
		quadrante = 1;
	}
	else if (roundf(next_p.getX()) > roundf(p.getX()) && roundf(next_p.getY()) < roundf(p.getY())) {
		quadrante = 2;
	}
	else if (roundf(next_p.getX()) > roundf(p.getX()) && roundf(next_p.getY()) > roundf(p.getY())) {
		quadrante = 3;
	}
	else if (roundf(next_p.getX()) < roundf(p.getX()) && roundf(next_p.getY()) > roundf(p.getY())) {
		quadrante = 4;
	}
	else {
		quadrante = 5;
	}

	//y = mx +b;
	GLfloat m, d;
	m = (next_p.getY() - p.getY()) / (next_p.getX() - p.getX());
	d = p.getY() - m * p.getX();

	GLfloat y = m * char_x + d;
	if (char_y >= (y - 0.1f) && char_y <= ( y + 0.1f)) {
		if (count == 1) {
			if (y >= (y_closer.at(0) - 0.5f) && y <= (y_closer.at(0) + 0.5f)) {
			}
			else {
				count++;
				y_closer.push_back(y);
				x_closer.push_back(char_x);
			}		
			if (count == 1) { //caso específico de reta paralela ao eixos dos x's
				if ((char_x >= (x_closer.at(0) - 0.5f) && char_x <= (x_closer.at(0) + 0.5f))) {												
				}
				else {
					count++;
					y_closer.push_back(y);
					x_closer.push_back(char_x);
				}
			}
		}
		else if (count == 0) {
			count++;
			y_closer.push_back(y);
			x_closer.push_back(char_x);
		}
	}

	if (count == 2) {
		if (quadrante == 3) { /*quadrantes foram trocados.*/
			if (y_closer.at(0) >= y_closer.at(1)) {
				wanted_y = y_closer.at(0);
			}
			else {
				wanted_y = y_closer.at(1);
			}
			if (x_closer.at(0) >= x_closer.at(1)) {
				wanted_x = x_closer.at(0);
			}
			else {
				wanted_x = x_closer.at(1);
			}
			count++;
		}
		else if (quadrante == 4) {
			if (y_closer.at(0) >= y_closer.at(1)) {
				wanted_y = y_closer.at(0);
			}
			else {
				wanted_y = y_closer.at(1);
			}
			if (x_closer.at(0) >= x_closer.at(1)) {
				wanted_x = x_closer.at(1);
			}
			else {
				wanted_x = x_closer.at(0);
			}
			count++;
		}
		else if (quadrante == 1) {
			if (y_closer.at(0) >= y_closer.at(1)) {
				wanted_y = y_closer.at(1);
			}
			else {
				wanted_y = y_closer.at(0);
			}
			if (x_closer.at(0) >= x_closer.at(1)) {
				wanted_x = x_closer.at(0);
			}
			else {
				wanted_x = x_closer.at(1);
			}
			count++;
		}
		else if (quadrante == 2) {
			if (y_closer.at(0) >= y_closer.at(1)) {
				wanted_y = y_closer.at(1);
			}
			else {
				wanted_y = y_closer.at(0);
			}
			if (x_closer.at(0) >= x_closer.at(1)) {
				wanted_x = x_closer.at(1);
			}
			else {
				wanted_x = x_closer.at(0);
			}
			count++;
		}
		else
			if (quadrante == 5) {
				wanted_y = y_closer.at(0);
				wanted_x = x_closer.at(1);
				count++;
			}
	}
	if (count == 3) {

		if ((char_y <= wanted_y + 0.5f && char_y >= wanted_y - 0.5f) && (char_x <= wanted_x + 0.5f && char_x >= wanted_x - 0.5f)) { //current position char.
			wanted_x = 0.0f;
			wanted_y = 0.0f;
			count = 0;
			y_closer.clear();
			x_closer.clear();
			wanted_x = NULL;
			wanted_y = NULL;
			return true;
		}
	}
	return false;
}

/*This methods calculates de line equation between two points.*/
void Collider::line_equation(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2, GLfloat &m, GLfloat &b) {
	//y = mx + b;
	m = (y2 - y1) / (x2 - x1);
	b = y1 - m * x1;
}

GLfloat Collider::checkCharacterPositionRelativeToPOIAndReturnsZ(GLfloat character_x, GLfloat character_y, GLfloat character_z, vector<POI> pois_list, vector<Road> roads_list) {
	GLfloat poi_x, poi_y, poi_z, poi_radius;

	vector<POI> ::iterator it;

	vector<Road> ::iterator rt;
	for (rt = roads_list.begin(); rt != roads_list.end(); rt++) {
		GLfloat test = rt->is_the_point_inside_returnz(character_x, character_y);
		if (test != -1 && character_z - RANGE_Z < test && character_z + RANGE_Z > test) {
			return test;
		}
	}

	for (it = pois_list.begin(); it != pois_list.end(); it++) {

		poi_x = it->getX();
		poi_y = it->getY();
		poi_z = it->getZ();
		poi_radius = it->getRadius();

		GLfloat value = sqrtf((pow(abs(character_x - poi_x), 2)) + (pow(abs(character_y - poi_y), 2))) - 5;
		if (value < 0 && character_z - RANGE_Z < poi_z && character_z + RANGE_Z > poi_z) {
			return poi_z;
		}
	}

	return 0.0;
}

BOOLEAN Collider::checkPostionIsInsideGrid(GLfloat c_x, GLfloat c_y, vector<POI> pois_list, vector<Road> roads_list) {

	GLfloat poi_x, poi_y, poi_z, poi_radius;
	vector<POI> ::iterator it;

	for (it = pois_list.begin(); it != pois_list.end(); it++) {

		poi_x = it->getX();
		poi_y = it->getY();
		poi_z = it->getZ();
		poi_radius = it->getRadius();

		GLfloat value = sqrtf((pow(abs(c_x - poi_x), 2)) + (pow(abs(c_y - poi_y), 2))) - 5;
		if (value < 0) {
			return true;
		}
	}

	vector<Road> ::iterator rt;
	for (rt = roads_list.begin(); rt != roads_list.end(); rt++) {
		if (rt->is_the_point_inside(c_x, c_y)) {
			return true;
		}
	}


	return false;
}

