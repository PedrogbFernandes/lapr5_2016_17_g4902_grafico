#include "Camera.h"

Camera::Camera() {
	/*Don't setup a Camera using this.*/
	this->vec_position[0] = 0.0f;
	this->vec_position[1] = 0.0f;
	this->vec_position[2] = 0.0f;
	this->vec_lookAt[0] = 0.0f;
	this->vec_lookAt[1] = 0.0f;
	this->vec_lookAt[2] = 0.0f;
	this->vec_orientation[0] = 0.0f;
	this->vec_orientation[1] = 0.0f;
	this->vec_orientation[2] = 1.0f;
}
/*
Camera instance with distance from the player and pitch angle.*/
Camera::Camera(GLfloat distance_from_player, GLfloat pitch_angle, bool orientation) {
	this->vec_position[0] = 0.0f;
	this->vec_position[1] = 0.0f;
	this->vec_position[2] = 0.0f;

	this->camera_distance_from_objective = distance_from_player;
	this->pitch = pitch_angle;

	this->vec_orientation[1] = 0.0f;
	if (orientation) {
		this->vec_orientation[0] = 0.0f;
		this->vec_orientation[1] = 1.0f;
		this->vec_orientation[2] = 0.0f;
	}
	else {
		this->vec_orientation[0] = 0.0f;
		this->vec_orientation[2] = 1.0f;
	}
}

Camera::~Camera() {
}

void Camera::incrementLatitude(GLfloat inc) {
	this->latitude += inc;

	if (this->latitude > 180) {
		this->latitude = 180;
	}
	if (this->latitude < 0) {
		this->latitude = 0;
	}
}

void Camera::incrementLongitude(GLfloat inc) {
	this->longitude += inc;

	if (this->longitude > 360 || this->latitude < -360) {
		this->longitude = 0;
	}
}

void Camera::computeLatitude() {
	this->latitude_distance = (GLfloat)((100 + this->camera_distance_from_objective) * sin(rad(this->latitude)));
}

void Camera::incrementDistance(GLfloat inc) {
	GLfloat a = this->camera_distance_from_objective;
	a += inc;
	if (a < 2.0f) {
		a = 2.0f;
	}
	if (a > 80.0f) {
		a = 80.0f;
	} 
	this->camera_distance_from_objective = a;
}

/*Distance in x component from player to camera.*/
void Camera::computeHorizontalDistance() {
	this->horizontal_distance = (GLfloat)(this->camera_distance_from_objective * cos(rad(this->pitch)));
}

/*Distance in z component from the player to the camera.*/
void Camera::computeVerticalDistance() {
	this->vertical_distance = (GLfloat)(this->camera_distance_from_objective * sin(rad(this->pitch)));

}

void Camera::computeYawn(GLfloat playerTurnSpeed) {
	//this->yawn = 180 - playerTurnSpeed;
	this->yawn = playerTurnSpeed;
}

/*Computes current camera position.*/
void Camera::computePosition(GLfloat player_x_movement, GLfloat player_y_movement, GLfloat player_z_movement) {
	this->vec_position[0] = (GLfloat)(player_x_movement + this->horizontal_distance * cos(rad(this->yawn)));
	this->vec_position[1] = (GLfloat)(player_y_movement + this->horizontal_distance * sin(rad(this->yawn)));
	this->vec_position[2] = (GLfloat)(this->vertical_distance) +  player_z_movement;
}

/*Update Camera current Position.*/
void Camera::computeLookAtPosition(GLfloat playerX, GLfloat playerY, GLfloat playerZ) {
	this->vec_lookAt[0] = playerX;
	this->vec_lookAt[1] = playerY;
	this->vec_lookAt[2] = playerZ;
}

/*Looks at the current object position.*/
void Camera::updateCamera(GLfloat obj_speed, GLfloat obj_turnSpeed, GLfloat player_x_movement, GLfloat player_y_movement, GLfloat x_obj, GLfloat y_obj, GLfloat z_obj) {
	computeHorizontalDistance();
	computeVerticalDistance();
	computeYawn(obj_turnSpeed);
	computePosition(player_x_movement, player_y_movement, z_obj);
	computeLookAtPosition(x_obj, y_obj, z_obj);
	//computeLatitude();

	gluLookAt(this->vec_position[0], this->vec_position[1], this->vec_position[2],
		this->vec_lookAt[0], this->vec_lookAt[1], this->vec_lookAt[2] /*+ this->latitude_distance*/,
		this->vec_orientation[0], this->vec_orientation[1], this->vec_orientation[2]);
}
