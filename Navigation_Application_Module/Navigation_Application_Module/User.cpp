#include "User.h"

User::User(string token, string username, string bearer) {
	this->token = token;
	this->username = username;
	this->bearer_type = bearer;
}

User::~User() {
}

bool User::addPathToPathList(vector<int> path) {
	vector<vector<int>> ::iterator it;
	/*TODO: implement operator.*/
	for (it = path_list.begin(); it != path_list.end(); it++) {
		if (*(it) == path) {
			return false;
		}
	}
	path_list.push_back(path);
	return true;
}

string User::Token() {
	return this->token;
}

string User::Username() {
	return this->username;
}
