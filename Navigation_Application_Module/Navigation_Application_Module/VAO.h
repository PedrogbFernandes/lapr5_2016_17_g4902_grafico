#pragma once

#include "Resources.h"
#include "Shader.h"
#include "Light.h"
/*This Class represents the in memory model.*/
class VAO
{
private:
	/*Vertex Array Objects ID*/
	int vaoID;
	/*Number of vertex used.*/
	int Nvertex;
	/*Texture ID associated with the VBO.*/
	int textureID;

public:
	VAO(int vao_id, int n_vertex, int texture_id);
	~VAO();
	int getVAOid();
	int getNVertex();
	int getTextureid();
	void render(Shader &shader,Light &light);

};

