#pragma once
#include "Resources.h"
#include "POI.h"

class User
{

private:

	string token;
	string username;
	string bearer_type;
	/*Path list with POI's ids.*/
	vector <vector<int>> path_list;

public:
	User(string token, string username, string bearer);
	~User();
	bool addPathToPathList(vector<int> path);
	string Token();
	string Username();
};

