//#include "Resources.h"

#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <codecvt>

class WebRequest
{
private:

public:
	static void PoisHTTPRequest(const char* filename);
	static void LoginHttpRequest(const char* filename, char* username, char* password);
	static void PathHttpRequest(const char* filename, char* token, char *username);
	static bool deleteFile(const char* filename);
};

