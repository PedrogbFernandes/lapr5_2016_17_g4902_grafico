#include "VAO.h"

VAO::VAO(int vao_id, int n_vertex, int texture_id) {
	this->vaoID = vao_id;
	this->Nvertex = n_vertex;
	this->textureID = texture_id;
}

VAO::~VAO() {
}

int VAO::getVAOid() {
	return this->vaoID;
}

int VAO::getNVertex() {
	return this->Nvertex;
}

int VAO::getTextureid() {
	return this->textureID;
}

void VAO::render(Shader &shader, Light &light) {
	shader.start();
	GLfloat ProjectionMatrix[16];
	GLfloat ModelViewMatrix[16];

	/*Retrieving current Light Position.*/
	//GLfloat LightPositionVec[3];
	//LightPositionVec[0] = light.getPosition().x;
	//LightPositionVec[1] = light.getPosition().y;
	//LightPositionVec[2] = light.getPosition().z;

	//GLfloat LightColourVec[3];
	//LightColourVec[0] = light.getColour().x;
	//LightColourVec[1] = light.getColour().y;
	//LightColourVec[2] = light.getColour().z;

	/*Retrieving and assigning matrices.*/
	glGetFloatv(GL_PROJECTION_MATRIX, ProjectionMatrix);
	glGetFloatv(GL_MODELVIEW_MATRIX, ModelViewMatrix);
	GLint modelview = glGetUniformLocation(shader.getProgramID(), "ModelViewMatrix");
	GLint projection = glGetUniformLocation(shader.getProgramID(), "ProjectionMatrix");

	//GLint light_position = glGetUniformLocation(shader.getProgramID(), "LightPosition");
	//GLint light_colour = glGetUniformLocation(shader.getProgramID(), "LightColour");

	glUniformMatrix4fv(projection, 1, GL_FALSE, ProjectionMatrix);
	glUniformMatrix4fv(modelview, 1, GL_FALSE, ModelViewMatrix);

	//glUniform3f(light_position, LightPositionVec[0], LightPositionVec[1], LightPositionVec[2]);
	//glUniform3f(light_colour, LightColourVec[0], LightColourVec[1], LightColourVec[2]);

	glBindVertexArray(this->vaoID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	//glEnableVertexAttribArray(2);
	glActiveTexture(GL_TEXTURE0);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glBindTexture(GL_TEXTURE_2D, this->textureID);
	glDrawArrays(GL_TRIANGLES, 0, this->Nvertex);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	//glDisableVertexAttribArray(2);
	glBindVertexArray(0);
	shader.stop();
}
