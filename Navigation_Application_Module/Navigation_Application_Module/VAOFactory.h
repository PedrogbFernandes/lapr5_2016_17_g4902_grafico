#pragma once

#include "Resources.h"
#include "Texture.h"
#include "TextureFactory.h"
#include "VAO.h"
#define VAOsize 1

#define VERTEX_SIZE 3
#define TEXTURE_SIZE_VERTEX 2
/*This class stores data into VBO and the VBO into a VAO.*/
class VAOFactory
{
private:
	VAOFactory();

	static vector<GLuint> VAOs;
	static vector<GLuint> VBOs;
	static vector<GLuint> textures;
	static int createVAO();
	static void bindVBOintoAttributeList(int index, vector<GLfloat> data, bool texture);
	static void bindVBOIndexBuffer(vector<GLuint> data, bool texture);
	static void unbindVAO();
	static float* allocateFloatBuffer(vector<GLfloat> data);
	static GLuint* allocateIntBuffer(vector<GLuint> data);
	static GLuint loadTexture(const char *filename);

public:
	//~VAOFactory();
	static VAO* loadDatatoVAO(const char *texture, vector<GLfloat> vertex_data, vector<GLfloat> texture_data, vector <GLfloat> normal_data, vector<GLuint> index_data);
	static void deleteData();
	//static VAO* loadDatatoVAOsec(GLfloat vertices[]);


};

