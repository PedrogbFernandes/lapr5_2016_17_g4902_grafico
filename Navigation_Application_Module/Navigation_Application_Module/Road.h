#pragma once
#include "POI.h"
#include "Resources.h"

// do not change.
static int const RADIUS = 1.0; 
class Road {
private:

	static vector<Road> Road_container;

	int poi_origem_id;
	int poi_destino_id;

	RoadRef a;
	RoadRef b;
	GLfloat distance_between_pois;
	GLfloat angle_between_pois;

	void computePOIsDistance();
	void computePOIsAngle();
	void computeLineEquation();
	void compute2DegreeEquation(GLfloat inv_m, GLfloat k, GLfloat a, GLfloat b, Point &sol1, Point &sol2, GLfloat &distance);
	void compute2DegreeEquationNoDeclive(GLfloat y, GLfloat a, GLfloat b, Point &sol1, Point &sol2, GLfloat &distance);
	GLfloat computeYBasedonXPosition(GLfloat x, GLfloat m, GLfloat k);
	GLfloat computeYBasedonXPositionNoDeclive(GLfloat x, GLfloat y);
	bool checkInterceptionWithBase(GLfloat x, GLfloat y, GLfloat a, GLfloat b);
	void computeRoadVertex(GLfloat &x, GLfloat &y, GLfloat m, GLfloat k, GLfloat a, GLfloat b, GLfloat sum_rate);

	/*Computes the new dot which will connect to p1 and p2.*/
	void compute_middle_dot(GLfloat a, GLfloat b, GLfloat m, GLfloat radius, GLfloat &x, GLfloat &y);
	void compute_middle_dot_no_declive(GLfloat a, GLfloat b, GLfloat radius, GLfloat &x, GLfloat &y);


public:

	BOOLEAN is_the_point_inside(GLfloat x, GLfloat y);
	GLfloat is_the_point_inside_returnz(GLfloat x, GLfloat y);
	Road(POI origem, POI destino);
	void render();
	int getOrigemID();
	int getDestinoID();

	static bool addRoad(Road road);
};
