#include "Texture.h"

Texture::Texture(const char * filename, int textureID){
	this->filename = filename;
	this->textureID = textureID;
}

Texture::~Texture() {
}

char* Texture::getFilename() {
	char * file = (char*) this->filename;
	return file;
}

int Texture::getTextureID() {
	return this->textureID;
}
