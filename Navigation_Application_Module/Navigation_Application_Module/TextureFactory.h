#pragma once
#include "Resources.h"
#include "Texture.h"
#include "SOIL\soil\soil.h"
class TextureFactory
{
	TextureFactory();
public:
	static Texture* loadTexture(const char *filename);
};

