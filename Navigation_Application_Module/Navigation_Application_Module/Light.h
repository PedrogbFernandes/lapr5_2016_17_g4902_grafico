#pragma once

#include "Resources.h"
class Light
{

private:
	
	Point position;
	Point colour;

public:
	Light(Point position, Point colour);
	~Light();

	void updatePosition(GLfloat x, GLfloat y, GLfloat z);
	void updateColour(GLfloat x, GLfloat y, GLfloat z);
	Point getPosition();
	Point getColour();
};

