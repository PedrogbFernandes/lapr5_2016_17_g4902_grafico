#include "Light.h"



Light::Light(Point position, Point colour) {
	this->position = position;
	this->colour = colour;
}

Light::~Light() {
}

void Light::updatePosition(GLfloat x, GLfloat y, GLfloat z) {
	this->position.x = x;
	this->position.y = y;
	this->position.z = z;
}

void Light::updateColour(GLfloat x, GLfloat y, GLfloat z) {
	this->colour.x = x;
	this->colour.y = y;
	this->colour.z = z;
}

Point Light::getPosition() {
	return this->position;

}
Point Light::getColour() {
	return this->colour;
}
