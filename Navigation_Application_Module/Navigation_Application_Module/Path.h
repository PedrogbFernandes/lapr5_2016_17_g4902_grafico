#pragma once
#include "Resources.h"
#include "POI.h"
#include "User.h"
using namespace std;

class Path
{

private:

	/*User who created this path.*/
	User *user;
	string name;
	int id;
	/*path list with pois id.*/
	vector<int> pois_path;

public:
	Path(int ID, string name, vector<int> pois);
	~Path();
	void setUser(User *user);
	int getID();
	vector<int> getPath();
	string getName();
};

