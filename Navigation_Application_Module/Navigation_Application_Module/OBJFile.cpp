#include "OBJFile.h"

vector<float> OBJFile::parseString(string line) {
	vector<float> p;
	int j = 0;
	int l;
	for (size_t i = 0; i < line.size() + 1; i++) {
		if (line[i] == delimiter || line[i] == '\0') {
			l = i - j;
			string aux = line.substr(j, l);
			j = i + 1;
			if (regex_match(aux.substr(0, 1), regex("-?|[0-9]"))) {
				string::size_type end;
				/*converting string to float data.*/
				float n = stof(aux, &end);
				p.push_back(n);
			}
		}
	}
	return p;
}

Triangle_reference OBJFile::parsesFString(string line) {
	Triangle_reference p;
	int j = 0, b = 0;
	for (size_t i = 0; i < line.size() + 1; i++) {
		if (line[i] == delimiter || line[i] == '\0') {
			int l = i - j;
			string aux = line.substr(j, l);
			j = i + 1;
			if (regex_match(aux.substr(0, 1), regex("-?|[0-9]"))) {
				vector<int> v_vertex;
				Vertex_reference v;
				int z = 0;
				for (size_t t = 0; t < aux.size() + 1; t++) {
					if (aux[t] == delimiter_2 || aux[t] == '\0') {
						int lc = t - z;
						string aux_2 = aux.substr(z, lc);
						z = t + 1;
						string::size_type end;
						int n = stoi(aux_2, &end);
						v_vertex.push_back(n);
					}
				}
				v.vertex = v_vertex.at(0);
				v.texture = v_vertex.at(1);
				v.normal = v_vertex.at(2);
				p.connection_reference[b] = v;
				b++;
			}
		}
	}
	return p;
}

Data OBJFile::readObjFile(const char *filename) {
	/*This is the data that will be stored in a vbo.*/
	Data data_file; 
	ObjFileData obj_file_data;
	string line;
	ifstream objfilename(filename);

	if (objfilename.is_open()) {
		while (getline(objfilename, line)) {
			vector<float> vertex_info;
			Triangle_reference t;
			string aux = line.substr(0, 2);
			Point p;
			Point2f p2;
			if (aux == V) { // its a vertex.
				/*Retrieves X,Y,Z coordinates of each vertex.*/
				vertex_info = OBJFile::parseString(line);
				p.x = vertex_info.at(X);
				p.y = vertex_info.at(Y);
				p.z = vertex_info.at(Z);
				obj_file_data.obj_vertex.push_back(p);

			}
			else if (aux == VT) { // its a texture vertex
				vertex_info = OBJFile::parseString(line);
				p2.x = vertex_info.at(X);
				p2.y = vertex_info.at(Y);
				obj_file_data.obj_texture_vertex.push_back(p2);

			}
			else if (aux == VN) { // its a normal from vertex
				vertex_info = OBJFile::parseString(line);
				p.x = vertex_info.at(X);
				p.y = vertex_info.at(Y);
				p.z = vertex_info.at(Z);
				obj_file_data.obj_normal_vertex.push_back(p);
			}
			else if (aux == F) { // its a relation, references of the vertex position will be extracted.
				t = parsesFString(line);
				data_file.data_indexed.push_back(t.connection_reference[0].vertex);
				data_file.data_indexed.push_back(t.connection_reference[1].vertex);
				data_file.data_indexed.push_back(t.connection_reference[2].vertex);
				data_file.texture_data_indexed.push_back(t.connection_reference[0].texture);
				data_file.texture_data_indexed.push_back(t.connection_reference[1].texture);
				data_file.texture_data_indexed.push_back(t.connection_reference[2].texture);
				data_file.normal_data_indexed.push_back(t.connection_reference[0].normal);
				data_file.normal_data_indexed.push_back(t.connection_reference[1].normal);
				data_file.normal_data_indexed.push_back(t.connection_reference[2].normal);
			}
		}
		objfilename.close();
	}
	else {
		cout << "Unable to open the selected file" << endl;
	}
	computeVertexAttributeList(data_file,obj_file_data);
	return data_file;
}

/*Returns false if de index value already exists in the vector.*/
bool OBJFile::checkReference(vector<GLuint> &data, GLuint index) {
	if (find(data.begin(), data.end(), index) != data.end()) {
		return false;
	}
	return true;
}

void OBJFile::computeVertexAttributeList(Data &data_file, ObjFileData &obj_file_data) {
	vector<GLuint>::iterator it;
	/*Per each vertex.*/
	for (it = data_file.data_indexed.begin(); it != data_file.data_indexed.end(); it++) {
		data_file.data_disposed.push_back(obj_file_data.obj_vertex.at(*(it) - 1).x);
		data_file.data_disposed.push_back(obj_file_data.obj_vertex.at(*(it) - 1).z);
		data_file.data_disposed.push_back(obj_file_data.obj_vertex.at(*(it) - 1).y);
	}

	for  (it = data_file.texture_data_indexed.begin(); it != data_file.texture_data_indexed.end(); it++) {
		data_file.texture_data_disposed.push_back(obj_file_data.obj_texture_vertex.at(*(it) - 1).x);
		data_file.texture_data_disposed.push_back(obj_file_data.obj_texture_vertex.at(*(it) - 1).y);
	}

	for (it = data_file.normal_data_indexed.begin(); it != data_file.normal_data_indexed.end(); it++) {
		data_file.normal_data_disposed.push_back(obj_file_data.obj_normal_vertex.at(*(it)-1).x);
		data_file.normal_data_disposed.push_back(obj_file_data.obj_normal_vertex.at(*(it)-1).z);
		data_file.normal_data_disposed.push_back(obj_file_data.obj_normal_vertex.at(*(it)-1).y);
	}
}