#pragma once
#include "Resources.h"

#define V "v "
#define VT "vt"
#define VN "vn"
#define F "f "

/*According to obj file type.*/
#define X 0
#define Y 1
#define Z 2
using namespace std;

class OBJFile
{

private:
	OBJFile();
	
	static const char delimiter =  ' ' ;
	static const char delimiter_2 = '/';
	static vector<float> parseString(string line);
	static Triangle_reference parsesFString(string line);
	static bool checkReference(vector<GLuint> &data, GLuint index);
	static void computeVertexAttributeList(Data &data_file, ObjFileData &obj_file_data);
public:
	//~OBJFile();
	static Data readObjFile(const char *filename);
};

