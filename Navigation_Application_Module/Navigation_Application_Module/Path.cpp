#include "Path.h"

Path::Path(int ID, string name, vector<int> pois) {
	this->id = ID;
	this->name = name;
	this->pois_path = pois;

}

Path::~Path() {
}

void Path::setUser(User *user) {
	this->user = user;
}

int Path::getID() {
	return this->id;
}

vector <int> Path::getPath() {
	return this->pois_path;
}

string Path::getName() {
	return this->name;
}