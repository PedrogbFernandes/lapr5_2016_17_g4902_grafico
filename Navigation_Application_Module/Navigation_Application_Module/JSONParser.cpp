#include "JSONParser.h"

vector<string> JSONParser::poi;
vector<vector<string>> JSONParser::pois;
bool JSONParser::handleConnections = false;
bool JSONParser::handleHashTags = false;
string JSONParser::poi_connected_ids = "";
string JSONParser::poi_hashtags = "";

const std::locale empty_locale = std::locale::empty();
typedef std::codecvt_utf8<wchar_t> converter_type;
const converter_type* converter = new converter_type;
const std::locale utf8_locale = std::locale(empty_locale, converter);

bool JSONParser::deletefile(const char * filename) {
	if (remove(filename) != 0) {
		cout << "Couldn't delete File" << endl;
		return false;
	}
	else {
		cout << "Success" << endl;
		return true;
	}
}

int JSONParser::handlePOIconnections(int ret, string &line) {
	int j = 0;
	int c = ret;
	string result;
	while (true) {
		if (line[c] == ']' || line[c] == '\0') { //when poi connections array ends.
			result = line.substr(ret, j);
			checkReference(result);
			break;
		}
		else {
			j++;
			c++;
		}
	}
	poi.push_back(poi_connected_ids);
	poi_connected_ids = "";
	handleConnections = false;
	return c;
}

int JSONParser::handleHashtags(int ret, string &line) {
	int j = 0;
	int c = ret;
	string result;
	while (true) {
		if (line[c] == ']' || line[c] == '\0') { //when poi hashtags connections array ends.
			result = line.substr(ret, j);
			checkReference(result);
			break;
		}
		else {
			j++;
			c++;
		}
	}

	poi.push_back(poi_hashtags);
	poi_hashtags = "";
	handleHashTags = false;
	return c;
}

int JSONParser::checkReference3(int ret, string &line) {
	int j = 0;
	int c = ret;
	string result;
	char delimiter_str;
	char delimiter;
	if (handleHashTags == true) {
		delimiter = '}';
	}
	else {
		delimiter = ',';
	}

	while (true) {
		delimiter_str = line[c];
		if (delimiter_str == delimiter ||  delimiter_str == '\0') {
			result = line.substr(ret, j);
			if (handleHashTags == true || handleConnections == true) {
				if (handleConnections == true && handleHashTags == false) {
					poi_connected_ids += result;
					poi_connected_ids += ";";
					//cout << "This is one stuff: " << result << endl;
				}
				if (handleConnections == false && handleHashTags == true) {
					poi_hashtags += result;
					poi_hashtags += ";";
					//cout << "This is one stuff: " << result << endl;
				}
			}
			else {
				//cout << "This is one stuff: " << result << endl;
				poi.push_back(result);
			}
			break;
		}
		else {
			j++;
			c++;
		}
	}
	return c;
}

int JSONParser::checkReference2(int c, string ref, string &line) {
	string aux = "";
	for (size_t i = 0; i < ref.size(); i++) {
		aux += line[c];
		c++;
	}
	if (aux == ref) {
		return c + 2; //remover as segundas " e os :
	}
	return 0;
}

void JSONParser::checkReference(string &line) {
	string reference;
	int ret;
	int end_control_max_index = 10;
	for (size_t i = 0; i < line.size(); i++) {

		if (i + end_control_max_index == line.size()) { //breaking the loop if there's no more tags to check in.
			break;
		}

		if (handleConnections == true || handleHashTags == true) { // if its only handle the poi connections, it's only required to check its ID.
			if (handleConnections == true && handleHashTags == false) {
				ret = checkReference2(i, ID, line);
				if (ret != 0) { // coincide.
					i = checkReference3(ret, line);
				}
			}
			else if (handleConnections == false && handleHashTags == true) {
				if (line.size() < 2) {
					break;
				}
				ret = checkReference2(i, Text, line);
				if (ret != 0) { // coincide.
					i = checkReference3(ret, line);
				}
			}
		}
		else {
			ret = checkReference2(i, ID, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, Name, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, Description, line);
			if (ret != 0) { 
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, OpenHour, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, CloseHour, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, GPS_Lat, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, GPS_Long, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, Altitude, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, Creator, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, Approved, line);
			if (ret != 0) {
				i = checkReference3(ret, line);
			}
			ret = checkReference2(i, ConnectedPOI, line);
			if (ret != 0) {
				handleConnections = true;
				i = handlePOIconnections(ret, line);
			}
			ret = checkReference2(i, Hashtags, line);
			if (ret != 0) {
				handleHashTags = true;
				i = handleHashtags(ret, line);
				break;
			}
		}	
	}
}

vector<POI> JSONParser::generatePOIS() {
	
	vector<POI> vec;
	vector<vector<string>> ::iterator main_it;
	vector<string>::iterator sec_it;

	size_t end;
	int _id;
	string _name;
	string _description;
	float lat;
	float lon;
	float alt;
	string _creator;
	string _approved;
	vector<string> _connectedPOIs;
	vector<int> connectedPoisID;
	vector<string> hashtags_poi;

	for (size_t i = 1; i < pois.size(); i++) { //per each vector
		for (size_t j = 0; j < pois.at(i).size(); j++) { // per each element of each vector.
			//cout << pois.at(i).size();
			if (pois.at(0).at(j) == ID) { // id 
				_id = stoi(pois.at(i).at(j), &end);
			}
			if (pois.at(0).at(j) == Name) { // poi name.
				_name = pois.at(i).at(j);
			}
			if (pois.at(0).at(j) == Description) { // pois desc.
				_description = pois.at(i).at(j);
			}
			if (pois.at(0).at(j) == GPS_Lat) { 
				lat = stof(pois.at(i).at(j), &end);
			}
			if (pois.at(0).at(j) == GPS_Long) { 
				lon = stof(pois.at(i).at(j), &end);
			}
			if (pois.at(0).at(j) == Altitude) {
				alt = stof(pois.at(i).at(j), &end);
				alt = alt * 0.1f;
			}
			if (pois.at(0).at(j) == ConnectedPOI) {
				string conn = pois.at(i).at(j);
				for (size_t v = 0; v < conn.size(); v++) {
					int size = conn.size();
					int c = v;
					int b = 0;
					while (true) {
						if (conn[v] == ';') {
							string l = conn.substr(c, b);
							connectedPoisID.push_back(stoi(l, &end));
							break;
						}
						else {
							v++;
							b++;
						}
					}
				}
			}
			if (pois.at(0).at(j) == Hashtags) {
				string hash = pois.at(i).at(j);
				for (size_t v = 0; v < hash.size(); v++) {
					int size = hash.size();
					int c = v;
					int b = 0;
					while (true) {
						if (hash[v] == ';') {
							string l = hash.substr(c, b);
							hashtags_poi.push_back(l);
							break;
						}
						else {
							v++;
							b++;
						}
					}
				}
			}
		}

		POI *poi = new POI(_id,_name, lat, lon, alt, _description);
		poi->setConnections(connectedPoisID);
		poi->setHashtags(hashtags_poi);
		POI::addPOI(*(poi));
		connectedPoisID.clear();
		hashtags_poi.clear();
		vec.push_back(*poi);
	}
	return vec;
}

void JSONParser::fillReferences() {
	vector<string> ref_vec;
	ref_vec.push_back(ID);
	ref_vec.push_back(Name);
	ref_vec.push_back(Description);
	ref_vec.push_back(OpenHour);
	ref_vec.push_back(CloseHour);
	ref_vec.push_back(GPS_Lat);
	ref_vec.push_back(GPS_Long);
	ref_vec.push_back(Altitude);
	ref_vec.push_back(Creator);
	ref_vec.push_back(Approved);
	ref_vec.push_back(ConnectedPOI);
	ref_vec.push_back(Hashtags);
	pois.push_back(ref_vec);
}

vector<POI> JSONParser::handlePOIstring(vector<vector<int>> &references, string &line) {
	string poi_string = "";
	vector <vector<int>>::iterator index_pois_iterator;
	vector<int> ::iterator index_solo_poi_iterator;
	vector<int> aux;
	int begin_poi, end_poi;

	for (index_pois_iterator = references.begin(); index_pois_iterator != references.end(); index_pois_iterator++) {

		aux = *(index_pois_iterator);
		index_solo_poi_iterator = aux.begin();
		begin_poi = *(index_solo_poi_iterator);
		end_poi = *(index_solo_poi_iterator + 1);

		for (int i = begin_poi; i < end_poi; i++) {
			poi_string += line[i];
		}

	
		/*cout << poi_string << endl;
		cout << endl;*/

		/*Creating POIs by each poi_string info.*/
		checkReference(poi_string); // pega na linha que diz respeito a apenas a um poi e extra� a informa��o.

		poi_string = "";
		pois.push_back(poi); // pois e poi � uma static var da classe.
		poi.clear();
	}
	return generatePOIS();
}

vector<POI> JSONParser::parseFile(const char * filename){
	fillReferences();

	string delimiter = "]},{";  // end of one poi info.
	int initial_poi_index = 0;
	int end_poi_index;
	int j = 0;
	bool flag = true;
	 /*Matrix de inteiros que faz refer�ncia ao index inicial de um POI e ao index final do mesmo POI*/
	vector<vector<int>> index_references;
	vector<int> index_solo_ref;
	string line;
	string delimiter_str = "";
	char end_line_str;
	ifstream jsonfile(filename); 
	jsonfile.imbue(utf8_locale);
	
	if (jsonfile.is_open()) {
		/*checks if the file is empty.*/
		if (jsonfile.peek() == ifstream::traits_type::eof()) {
			throw std::invalid_argument("Request Failed.");
		}
		else {
			while (getline(jsonfile, line)) {
				for (size_t i = 0; i < line.size(); i++) {

					delimiter_str = line[i];  //]
					delimiter_str += line[i + 1]; //{
					delimiter_str += line[i + 2]; //,
					delimiter_str += line[i + 3]; //]
					end_line_str = line[i + 3]; //\0

					if (line[i] == '{' && flag == true) { //apanhou um POI.
						flag = false;
						initial_poi_index = i;

						//cout << "Ponto inicial: " << initial_poi_index << endl;

						j = initial_poi_index;
						index_solo_ref.push_back(initial_poi_index);
					}
					else if ((delimiter_str == delimiter) || (end_line_str == '\0')) { // terminou a info do POI ou o ficheir n�o tem mais informa��o.
						flag = true;
						end_poi_index = j + 3;

						//cout << "Ponto final: " << end_poi_index << endl;
						//cout << endl;

						index_solo_ref.push_back(end_poi_index);
						index_references.push_back(index_solo_ref); // insere o index inicial e o index final de um poi.
						index_solo_ref.clear();
						if (end_line_str == '\0') { 
							break;
						}
					}
					else {
						j++;
					}
				}
			}
		}
		jsonfile.close();
	}
	else {
		cout << "Unable to Open File" << endl;
	}

	return handlePOIstring(index_references,line);
}

/*Parse Login File related Code.*/
int JSONParser::checkLoginRequestTokens(string &line, int c, string ref) {
	string aux = "";
	for (size_t i = 0; i < ref.size(); i++) {
		aux += line[c];
		c++;
	}
	if (aux == ref) {
		return c + 2; //remover as segundas " e os :
	}
	return 0;
}

string JSONParser::extractLoginInfo(string &line, int &c) {
	int j = c;
	bool add = false;
	char delimiter = ',';
	char aspas = '\"';
	string info = "";
	while (true) {
		if (line[j] == aspas) {
			if (add) {
				add = false;
				break;
			}
			else {
				add = true;
				j++;
			}
		}
		if (add) {
			info += line[j];
		}
		j++;
	}
	c = j;
	return info;
}

User* JSONParser::parseLoginfile(const char * filename) {

	//deletefile(filename);

	User *user;

	int j = 0;
	ifstream jsonfile(filename);
	jsonfile.imbue(utf8_locale);
	string line;
	string token, username, bearer;
	if (jsonfile.is_open()) {
		while (getline(jsonfile, line)) {
			for (size_t i = 0; i < line.size(); i++) {
				if (i + 10 == line.size()) {
					break;
				}
				j = checkLoginRequestTokens(line, i, Token); //retorna a posi��o da string depois de encontrar uma reference.
				if (j != 0) { //found.
					token = extractLoginInfo(line, j);
					i = j;
				}
				j = checkLoginRequestTokens(line, i, UserName); //retorna a posi��o da string depois de encontrar uma reference.
				if (j != 0) { //found.
					username = extractLoginInfo(line, j);
					i = j;
				}
				j = checkLoginRequestTokens(line, i, Bearer); //retorna a posi��o da string depois de encontrar uma reference.
				if (j != 0) { //found.
					bearer = extractLoginInfo(line, j);
					i = j;

				}
			}
		}
	}
	else {
		cout << "Couldn't Open File" << endl;
	}

	jsonfile.close();
	user = new User(token, username, bearer);

	if (token.size() == NULL) {
		return NULL;
	}
	return user;
}

string JSONParser::checkReferencePath(int &i, string &line, bool is_it_a_path) {
	string aux;
	int j = 0;
	int c = i;
	string result, delimiter2;
	char delimiter;
	if (is_it_a_path) {
		delimiter2 = "\","; //end of path.
		j += 1;
	}
	else {
		delimiter = ','; //end of any other att.
	}

	while (true) {
		if (is_it_a_path) {
			aux = line[c];
			aux += line[c + 1];
			if (aux == delimiter2) {
				result = line.substr(i, j); //to include the ".
				break;
			}
			else {
				j++;
				c++;
			}
		}
		else {
			if (line[c] == delimiter || line[c] == '\0') {
				result = line.substr(i, j); //to include the ".
				break;
			}
			else {
				j++;
				c++;
			}
		}	
	}

	i = c;
	return result;
}

vector<int> JSONParser::extractPathIDs(string &path_ids) {
	vector<int> path;
	size_t end;
	int b = 0;
	int id;
	int j = 0;
	int c = 1; //starting at 1 to ignore the ";
	b = c;
	string result;
	char delimiter;

	while (true) {
		char damn2 = path_ids[c];
		if (path_ids[c] == ',' || path_ids[c] == '\"') {
			result = path_ids.substr(b, j); //encotrou uma virgula n� ou encontrou a puta das aspas. sempre que contrar uma virgula, actualiza a posi��o em b
			char damn = path_ids[b];
			char damn1 = path_ids[c];
			char damn2= j;
			j = 0;
			b = c + 1;
			if (result.size() != NULL) {
				id = stoi(result, &end);
				path.push_back(id);
				//cout << id << endl;
			}
			if (path_ids[c] == '\"') {
				break;
			}
			c++;
		}
		else {
			j++;
			c++;
		}
	}
	return path;
}
/*Parse Path File related Code.*/
vector<Path> JSONParser::parsePathFile(const char* filename) {

	vector<Path> paths;
	vector<int> pois_path;
	bool add_path_flag = false;
	string id_str, path_ids, creator, path_name;
	int id;
	size_t end;
	int c = 0;
	string filename_s(filename);
	ifstream pathfile(filename);
	pathfile.imbue(utf8_locale);
	string line;
	if (pathfile.is_open()) {
		if (pathfile.peek() == ifstream::traits_type::eof()) { //file is empty.
			throw std::invalid_argument("Request Failed.");
		}
		while (getline(pathfile, line)) {
			for (size_t i = 0; i < line.size(); i++) {
				if (i + PercursoPOIsOrder.size() == line.size()) {
					break; //can't go all the way to the end.
				}
				else {
					c = checkReference2(i, ID, line); //checks if ID exists. c will be in a post position of where "ID:" is.
					if (c != 0) { // c must have the current index line position post "ID".
						id_str = checkReferencePath(c, line, false); //extracts info and updates i position, till a ',' is found.
						i = (size_t)c; //updates i index.
						id = stoi(id_str, &end); 		
					}

					c = checkReference2(i, Name, line);
					if (c != 0) {
						path_name = checkReferencePath(c, line, false);
						i = (size_t)c; //updates i index.
					}
					c = checkReference2(i, PercursoPOIsOrder, line);
					if (c != 0) {
						path_ids = checkReferencePath(c, line, true);
						i = (size_t)c;
						pois_path = extractPathIDs(path_ids);
						//cout << path_ids << endl;
						add_path_flag = true;
					}
					if (add_path_flag) {
						add_path_flag = false;
						Path *path = new Path(id, path_name, pois_path);
						pois_path.clear();
						paths.push_back(*path); //path was added, keep searching for other paths.
					}
				}
					
			}
		}
		pathfile.close();
	}
	else {
		cout << "Couldn't Open File: " << filename_s << endl;
	}
	return paths;
}