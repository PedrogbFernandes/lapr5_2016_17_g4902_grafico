#pragma once

#define _USE_MATH_DEFINES

#include <iostream>
#include <windows.h>
#include <thread>
#include <cmath>
#include <time.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <regex>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <locale>
#include <codecvt>
#include <string>
#include <fstream>
#include <cstdlib>
#include <locale>
#include <clocale>
#include <math.h>
#include "mathlib.h"
#include "GLEW\GL\glew.h"
#include "freeGLUT\freegl\freeglut.h"
#include "studio.h"
#include "mdlviewer.h"
#include <memory>
#include <complex>
#include "FLTK\FL\Fl.H"
#include "FLTK\FL\Fl_Window.H"
#include "FLTK\FL\Fl_Box.H"
#include "FLTK\FL\Fl_Widget.H"
#include "FLTK\FL\Fl_Button.H"
#include "FLTK\FL\Fl_Menu_Button.H"
#include "FLTK\FL\Fl_Input.H"
#include "FLTK\FL\Fl_Secret_Input.H"
#include "FLTK\FL\FL_Output.H"
#include "FLTK\FL\fl_ask.H"
#include "FLTK\FL\Fl_Text_Display.H"
#include "FLTK\FL\Fl_Text_Editor.H"
#include "OpenAL\AL\alut.h"
#include "vsync\wglext.h"
#include "vsync\glext.h"




#define degree(X) (double)((X)*180/M_PI)
#define rad(X)   (double)((X)*M_PI/180)
#define WIDTH_ON_TEXT(x)	(float)((x/5)*4)

#define SEC_PER_MIL 1000.0

#define MAP_MIN_X -8.726065
#define MAP_MAX_X -8.560989

#define MAP_MIN_Y 41.140150
#define MAP_MAX_Y 41.210131

#define X_PARSER 0.165076
#define Y_PARSER 0.069981
#define Y_MAX 435.2
#define X_MAX 769.6
#define poi_cube_size 6
#define MAPPING_TO_SCREEN 0
#define MAPPING_TO_WORLD 1
#define ANGLE_ROADS 45
#define RANGE_Z 5

using namespace std;

static string const BUILDING = "\"edificio\"";
static string const OLD_HOUSE = "\"casa\"";
static string const TREE = "\"ambiente\"";
static string const GRASS = "\"estadio\"";

typedef struct {
	GLfloat x;
	GLfloat y;
	GLfloat z;
}Point;

typedef struct {
	GLfloat x;
	GLfloat y;
}Point2f;

typedef struct {
	Point p1;
	Point p2;
	Point p3;
	Point p4;
	Point center;
	GLfloat radius;
}RoadRef;

/*This struct is required to work with .obj files*/
typedef struct {
	vector<Point> obj_vertex;
	vector<Point2f> obj_texture_vertex;
	vector<Point> obj_normal_vertex;
}ObjFileData;

typedef struct {
	int vertex;
	int texture;
	int normal;
}Vertex_reference;

typedef struct {
	Vertex_reference connection_reference[3];
}Triangle_reference;

typedef struct {
	vector<GLfloat> data;
	vector<GLuint> data_indexed;
	vector<GLfloat> data_disposed;
	vector<GLfloat> texture_data;
	vector<GLuint> texture_data_indexed;
	vector<GLfloat> texture_data_disposed;
	vector<GLfloat> normal_data;
	vector<GLuint> normal_data_indexed;
	vector<GLfloat> normal_data_disposed;
}Data;

class Resources {
private:
	/*Static concept*/
	Resources();
	
public:

	static void getMonitorResolution(int &width, int &height) {
		RECT desktop;
		/*Desktop handler achieved by Windows API*/
		const HWND window_handler = GetDesktopWindow();
		/*Saves current Desktop measurements into a Rect Structure.*/
		GetWindowRect(window_handler, &desktop);
		width = desktop	.right;
		height = desktop.bottom;
	}

	/*This method converts World Coordinates into Screen Coordinates or vice-versa.*/
	static void convertCoordinates(int mapping, GLfloat x, GLfloat y, GLfloat z, GLdouble *result_vertex) {
		/*Matrix Containers.*/
		GLdouble projection_matrix[16];
		GLdouble modelview_matrix[16];
		GLint viewport[4];
		/*Saving current Matrixes state.
		PIPE operation for Screen Coordinates.
		Backwards PIPE operation for World Coordinates.*/
		glGetDoublev(GL_PROJECTION_MATRIX, projection_matrix);
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview_matrix);
		glGetIntegerv(GL_VIEWPORT, viewport);

		if (mapping == MAPPING_TO_SCREEN) {
			gluProject(x, y, z, modelview_matrix, projection_matrix, viewport, result_vertex + 0,
				result_vertex + 1, result_vertex + 2);
		}
		else if (mapping == MAPPING_TO_WORLD) {
			gluUnProject(x, y, z, modelview_matrix, projection_matrix, viewport, result_vertex + 0,
				result_vertex + 1, result_vertex + 2);
		}
	}

	static string stringWithAccentsChangedToNormalLetters(const string& s)
	{
		string result = "";
		for (unsigned int i = 0; i < s.length(); i++)
		{
			unsigned char character = s[i];

			if (character >= 128)
			{
				if (character == 195 && i < s.length() - 1)
				{
					i++;
					unsigned char secondaryCharacter = s[i];
					vector<char> translation = { 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'X', 'O', 'U', 'U', 'U', 'U', 'Y', 'P', 'B', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', '/', 'o', 'u', 'u', 'u', 'u', 'y', 'p', 'y' };

					unsigned int index = secondaryCharacter - 128;

					if (secondaryCharacter >= 128 && index < translation.size())
					{
						result += translation[index];
					}
					else
					{
						result += '?';
					}
				}
				else
				{
					result += '?';
				}
			}
			else
			{
				result += s[i];
			}
		}
		return result;
	}


};
