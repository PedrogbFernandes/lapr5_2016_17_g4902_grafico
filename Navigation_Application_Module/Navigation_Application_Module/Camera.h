#pragma once

#include "Resources.h"
class Camera
{

private:

	/*Camera requirements.*/
	GLfloat vec_position[3];
	GLfloat vec_lookAt[3];
	GLfloat vec_orientation[3];

	/*mechanism not implemented for third person - to use later.*/
	GLfloat latitude;
	GLfloat longitude;
	GLfloat latitude_distance;

	bool camera_orientation;
	GLfloat horizontal_distance;
	GLfloat vertical_distance;
	GLfloat camera_distance_from_objective;
	GLfloat pitch;
	GLfloat yawn;

	void computeHorizontalDistance();
	void computeVerticalDistance();
	void computeYawn(GLfloat playerTurnSpeed);
	void computePosition(GLfloat player_x_movement, GLfloat player_y_movement, GLfloat player_z_movement);
	void computeLatitude();
	void computeLookAtPosition(GLfloat playerX, GLfloat playerY, GLfloat playerZ);
public:
	Camera();
	Camera(GLfloat distance_from_player, GLfloat pitch_angle, bool orientation);
	~Camera();

	void incrementDistance(GLfloat inc);
	void updateCamera(GLfloat obj_speed, GLfloat obj_turnSpeed, GLfloat player_x_movement, GLfloat player_y_movement, GLfloat x_obj, GLfloat y_obj, GLfloat z_obj);

	/*mechanism not being used for third person - to use later.*/
	void incrementLatitude(GLfloat inc);
	void incrementLongitude(GLfloat inc);
};

