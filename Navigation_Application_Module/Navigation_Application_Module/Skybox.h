#pragma once

#include "Resources.h"
#include "TextureFactory.h"
#include "Texture.h"

class Skybox
{

private:
	Texture *front;
	Texture *back;
	Texture *left;
	Texture *right;
	Texture *up;
	Texture *down;


public:
	Skybox();
	void render(GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height, GLfloat length);

};