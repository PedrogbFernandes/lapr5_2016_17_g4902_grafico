﻿#include "Resources.h"
#include "View.h"
#include "Camera.h"
#include "POI.h"
#include "Road.h"
#include "Collider.h"
#include "OBJFile.h"
#include "VAO.h"
#include "VAOFactory.h"
#include "Texture.h"
#include "TextureFactory.h"
#include "Shader.h"
#include "WebRequest.h"
#include "JSONParser.h"
#include "Skybox.h"
#include "User.h"
#include "Path.h"
#include "Weather.h"
#include "Light.h"
#include "Sound.h"
#include <freegl\freeglut.h>


typedef struct {
	GLfloat p1x, p1y;
	GLfloat p2x, p2y;
	GLfloat p3x, p3y;
	GLfloat p4x, p4y;

}Path_quad_reference;

typedef struct {
	GLfloat x, y, z;
	GLfloat radius = 10.0f;
	GLfloat angle = 0.0f;
}Sun;

typedef struct {
	GLboolean space, w, a, s, d, f, enter, t;
}Keyboard;

typedef struct {
	int previous_x;
	int previous_y;
}Mouse;

typedef struct {
	double deltaTime = 0;
	unsigned int frames = 0;
	double frameRate = 0;
	double avgFramebyMill = 0;
	clock_t beginFrame, endFrame;
	bool start;
}FPSCounter;

typedef struct {
	bool perspectiveFlag = true;
	bool orthogonalFlag = false;
	bool sidebarFlag = false;
}Projection;

typedef struct {
	GLfloat x, y, z = 0.0f;
	GLfloat speed = 0.0f;
	GLfloat turnSpeed;
	GLfloat x_movement;
	GLfloat y_movement;
	StudioModel model;
}Character;

typedef struct {
	View *view;
	Camera *camera, *camera2;
	Projection projection;
	Sun sun;
	Character character;
	Keyboard keys;
	vector<POI> poilist;
	vector<Road> roads;
	Collider *collider;

	GLfloat compare_z;

	/*This is related to auto path.*/
	vector<Path_quad_reference> quads_reference_poi;
	vector<POI> path_list;
	int path_number_ref = -1;
	int path_number = 0;
	bool go = false;
	string chosen_path;
	bool walking_on_poi;
	bool reached_destination;
	bool update_poi;

	/*VAO VBO related.*/
	VAO *old_house, *tree;
	VAO *building, *stadium;
	VAO *church;
	Shader *shader;
	Texture *sun_texture, *map_porto, *label, *ball_texture;

	/*This is Menu Related.*/
	Fl_Input *username;
	Fl_Input *password;
	User *user;
	bool user_flag;
	vector<Path> user_path_list;
	vector<int> buttons_pressed;
	vector<int> ids_pressed;
	char *user_char = NULL;
	char *password_char = NULL;
	Light *light;

}Model;

typedef struct {
	vector<string> poilist;
}Volatile;

typedef struct {
	Mouse mouse;
	FPSCounter fpscounter;
	GLint width;
	GLint height;
	float ModelViewMatrix[16];
	float ProjectionMatrix[16];
}State;

Model model;
State state;
Skybox *skybox;
int active_viewport, active_weather;
int width_f, height_f;
Volatile vol;
Weather *weather;
Sound *sound;
string note;
Texture *intro;
void menu_handler(int op);
GLfloat cnt1;                           // 1st Counter Used To Move Text & For Coloring
GLfloat cnt2;                           // 2nd Counter Used To Move Text & For Coloring

using namespace std;

void init_menu() {
	int menu;
	menu = glutCreateMenu(menu_handler);
	glutSetMenuFont(menu, GLUT_BITMAP_9_BY_15);
	glutAddMenuEntry("Log In", 1);
	glutAddMenuEntry("Path Selection", 2);
	glutAddMenuEntry("Log Out", 3);
	glutAddMenuEntry("Close", 4);

	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void init_light() {
	/*Source Light Vectores*/
	GLfloat source_light_ambient[] = { 0.7f, 0.7f, 0.7f, 0.7f };
	GLfloat source_light_diffuse[] = { 0.7f, 0.7f, 0.7f, 0.7f };
	GLfloat source_light_specular[] = { 0.7f, 0.7f, 0.7f, 0.7f };
	GLfloat light_source_position[] = { 769.6f / 8.0f, 435.2f / 2.0f, 50.0f, 0.0f };

	/*Apply Source Vectores.*/
	glLightfv(GL_LIGHT0, GL_AMBIENT, source_light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, source_light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, source_light_specular);
	//glLightfv(GL_LIGHT0, GL_POSITION, light_source_position);

	/*Material Light Vectores*/
	GLfloat material_light_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat material_light_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	GLfloat material_light_specular[] = { 0.5f, 0.5f, 0.5f, 1.0f };

	/*Apply Material Vectores*/
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_light_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_light_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_light_diffuse);

	/*Saves Lightning State.*/
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
}

void bootstrap_poi() {
	srand((unsigned int)time(NULL));
	for (int i = 0; i < 3; i++) {
		ostringstream aux;
		aux << (i);
		string des = aux.str();
		GLfloat randx = (GLfloat)(rand() % 100 + 1);
		GLfloat randy = (GLfloat)(rand() % 100 + 1);
		GLfloat randz = (GLfloat)(rand() % 100 + 1);
		GLfloat z = (GLfloat)(randz / 100 * poi_cube_size / 2 + 3);
		//GLfloat z = 0.0;
		GLfloat y = (GLfloat)(randy / 100 * Y_PARSER + MAP_MIN_Y);
		GLfloat x = (GLfloat)(randx / 100 * X_PARSER + MAP_MIN_X);
		POI *poi = new POI(i, des, y, x, z, des);
		/*pushing back pointed to.*/
		model.poilist.push_back(*poi);
	}
}

void bootstrap_road() {
	vector<POI> ::iterator it;
	for (size_t i = 0; i < model.poilist.size(); i++) {
		for (size_t j = 0; j < model.poilist.at(i).getConnections().size(); j++) { //per each connection.

			POI* orig = POI::getPOIbyID(model.poilist.at(i).getID());
			if (orig->getID() == 79) {
				cout << endl;
			}
			POI* dest = POI::getPOIbyID(model.poilist.at(i).getConnections().at(j));
			if (orig == NULL || dest == NULL) {
				cout << "Error creating Road" << endl;
			}
			Road* a = new Road(*(orig), *(dest));
			if (Road::addRoad(*a)) { //só adiciona uma road se a origem e o destino formarem uma ligação que ainda não foi criada.
				model.roads.push_back(*a);
			}
		}
	}
}

void init() {
	setlocale(LC_ALL, "Portuguese");
	init_light();
	init_menu();
	skybox = new Skybox();
	weather = new Weather();
	srand(time(NULL));
	active_weather = (int)(rand() % 3);

	mdlviewer_init("res/gman.mdl", model.character.model);
	model.map_porto = TextureFactory::loadTexture("res/mapa.tga");

	//bootstrap_poi();
	bootstrap_road();


	model.character.x = (GLfloat)model.poilist.begin()->getX();
	model.character.y = (GLfloat)model.poilist.begin()->getY();
	model.character.z = (GLfloat)model.poilist.begin()->getZ();

	weather->init(model.character.x, model.character.y, model.character.z);

	/*Clears all pixel.*/
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glShadeModel(GL_SMOOTH);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_NORMALIZE);
	/*Depth to the furthest point.*/
	glClearDepth(1.0f);
	glCullFace(GL_FRONT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
}

void fps_counter() {
	state.fpscounter.frames++;
	state.fpscounter.deltaTime += (state.fpscounter.endFrame - state.fpscounter.beginFrame);
	double timer = state.fpscounter.deltaTime / (double)CLOCKS_PER_SEC * (double)SEC_PER_MIL;

	if (timer > 1000) {
		state.fpscounter.frameRate = state.fpscounter.frames;
		state.fpscounter.avgFramebyMill = 1000.0 / state.fpscounter.frames;
		//cout << "FPS: " << state.fpscounter.frames << endl;
		//cout << "SPF: " << state.fpscounter.avgFramebyMill << endl;
		state.fpscounter.deltaTime -= CLOCKS_PER_SEC;
		state.fpscounter.frames = 0;
		state.fpscounter.start = true;
	}
}


void compute_path() {
	if (model.path_number < (int)model.path_list.size()) {

		/*Retrieving actual poi information.*/
		POI p = model.path_list.at(model.path_number);
		GLfloat new_char_x_position;
		GLfloat new_char_y_position;
		GLfloat radius = p.getRadius() - 1.0f;

		if (model.reached_destination == false) {
			if (model.path_number == 0) { // for the first path position.
				model.character.x = p.getX();
				model.character.y = p.getY();
				model.character.z = p.getZ();
				model.path_number++;
			}
			else {
				int next_index = model.path_number;
				if (next_index == (int)model.path_list.size()) {
					model.reached_destination = true;
				}
				else {
					POI next_p = model.path_list.at(next_index);
				}
				/*Vê se a character está dentro do poi para o qual se está a deslocar.*/
				if (model.collider->checkCharacterPositonInsideRadius(model.character.x, model.character.y, p, radius)) {
					int next_index = model.path_number + 1;

					if (next_index == (int)model.path_list.size()) {
						model.reached_destination = true;
						model.go = false;
						model.path_number = 0;
						model.reached_destination = false;
					}
					else {
						POI next_p = model.path_list.at(next_index);
						POI previous_p = model.path_list.at(next_index - 2);
						model.collider->computeNextPositionBasedonCircus(model.character.x, model.character.y,p, previous_p, radius, new_char_x_position, new_char_y_position);
						/*Se a posição atual da character pertencer a nova reta então o poi atual é actualizado.*/
						if (model.collider->ckeckCharacterBelongsToNewLine(model.character.x, model.character.y, p, next_p)) {
							model.path_number++;
						}
						/*Com estes novos valores é verificada a posição da personagem relativamente à nova.*/
						model.character.x = new_char_x_position;
						model.character.y = new_char_y_position;
					}
				}
				else { /*se a character não estiver dentro do poi para o qual se desloca então caminha sobre a reta até lá chegar.*/
					model.collider->checkNextLinePoint(model.character.x, model.character.y, p, new_char_x_position, new_char_y_position);
					model.character.x = new_char_x_position;
					model.character.y = new_char_y_position;
					/*if (round(model.character.x) == round(p.getX()) && round(model.character.y) == round(p.getY())) {
						model.path_number++;
					}*/
				}
			}
		}
	}
	else {
		/*model.character.model.SetSequence(0);*/
		model.go = false;
		model.path_number = 0;
		model.reached_destination = false;
	}
}

void render_character_position() {
	GLfloat aux_x = model.character.x + (GLfloat)(model.character.speed * cos(rad(model.character.turnSpeed)));
	GLfloat aux_y = model.character.y + (GLfloat)(model.character.speed * sin(rad(model.character.turnSpeed)));
	GLfloat aux_z = model.collider->checkCharacterPositionRelativeToPOIAndReturnsZ(model.character.x, model.character.y, model.character.z, model.poilist, model.roads) + 0.8f;
	BOOLEAN debug = model.collider->checkPostionIsInsideGrid(aux_x, aux_y, model.poilist, model.roads);
	if (debug) {
		model.character.x += (GLfloat)(model.character.speed * cos(rad(model.character.turnSpeed)));
		model.character.y += (GLfloat)(model.character.speed * sin(rad(model.character.turnSpeed)));
		model.character.z = model.collider->checkCharacterPositionRelativeToPOIAndReturnsZ(model.character.x, model.character.y, model.character.z, model.poilist, model.roads) + 0.8f;
	}
}

void timer(int t) {
	glutTimerFunc(1, timer, 1);

	/*Go Flag enables this and sets the current position of the character.*/
	/*This method will change character coordinates.*/
	render_character_position();

	//printf("Personagem x=%f,y=%f", model.character.x, model.character.y);

	if (model.go == true) {
		if (model.character.model.GetSequence() != 13) {
			model.character.model.SetSequence(13);
			sound->PlayStep();
		}
		compute_path();
	}

	if (model.character.speed == 0 && model.go == false) {
		model.character.model.SetSequence(0);
		sound->StopStep();
	}
	else {
		if (model.character.model.GetSequence() != 13) {
			model.character.model.SetSequence(13);
			sound->PlayStep();
		}
	}

	if (model.keys.f == GL_TRUE) {
		model.projection.sidebarFlag = true;
	}
	else {
		model.projection.sidebarFlag = false;
	}

	if (model.projection.perspectiveFlag == true) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		model.view->setPerspectiveView(30, 0.1f, 500.0f);
		//model.view->setOrthogonalView(25, width, height, -1000.0f, 1000.0f);
	}
	else if (model.projection.orthogonalFlag == true) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		model.view->setOrthogonalView(45, -1000.0f, 1000.0f);
	}

	if (model.keys.a == GL_TRUE) {
		model.character.turnSpeed += 0.30f;
		if (model.character.turnSpeed > 360) {
			model.character.turnSpeed = 0.0f;
		}
	}


	if (model.keys.d == GL_TRUE) {
		if (model.character.turnSpeed < -360) {
			model.character.turnSpeed = 0.0f;
		}
		model.character.turnSpeed -= 0.30f;
	}
	if (model.keys.w == GL_TRUE) {
		/*across negative x.*/
		model.character.speed = -0.1f;
	}
	else {
		model.character.speed = 0;
	}
	if (model.keys.s == GL_TRUE) {
		model.character.speed = +0.1f;
	}

	glutPostRedisplay();
}

void draw_Image(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLint text)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, text);
	glColor3f(0.5f, 0.5f, 0.5f);

	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2fv(a);
	glTexCoord2f(0.0, 1.0);
	glVertex2fv(b);
	glTexCoord2f(1.0, 1.0);
	glVertex2fv(c);
	glTexCoord2f(1.0, 0.0);
	glVertex2fv(d);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
}

void draw_axis() {
	glPushMatrix();
	glTranslatef(20.0f, 20.0f, 1.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(5.0f, 0.0f, 0.0f);
	glEnd();
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 5.0f, 0.0f);
	glEnd();
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 5.0f);
	glEnd();
	glPopMatrix();
}

void draw_floor() {

	GLfloat size_image_x = 3848;
	GLfloat size_image_y = 2176;

	Point p_1, p_2, p_3, p_4;
	p_1.x = 0.0f;
	p_1.y = 0.0f;
	p_1.z = -2;
	p_2.x = (size_image_x) * 0.2f;
	p_2.y = 0.0f;
	p_2.z = -2;
	p_3.x = (size_image_x)* 0.2f;
	p_3.y = (size_image_y)* 0.2f;
	p_3.z = -2;
	p_4.x = 0.0f;
	p_4.y = (size_image_y)* 0.2f;
	p_4.z = -2;

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);

	/*Combining env.*/
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	/*Fixing Deformations.*/
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Draw Front side
	glBindTexture(GL_TEXTURE_2D, model.map_porto->getTextureID());
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(p_1.x, p_1.y, p_1.z);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(p_2.x, p_2.y, p_2.z);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(p_3.x, p_3.y, p_3.z);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(p_4.x, p_4.y, p_4.z);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void renders_sun_trajectory(GLfloat timestep) {
	GLfloat inc = 360.0f * timestep / (24.0f * 60.0f * 60.0f);
	model.sun.angle += inc;
	if (model.sun.angle > 360) {
		model.sun.angle = 0;
	}
	model.sun.x = (GLfloat)(769.6 / 2.0f);
	model.sun.y = (GLfloat)((+435.2f / 2.0f + model.sun.radius) * sin(rad(model.sun.angle)));
	model.sun.z = (GLfloat)((+435.2f / 2.0f + model.sun.radius) * cos(rad(model.sun.angle)));
}


void draw_sun() {
	//GLfloat timestep = 1.0f;
	//glColor3f(1.0f, 0.5f, 0.0f);
	glPushMatrix();
	glTranslatef(model.sun.x, model.sun.y, model.sun.z);
	glEnable(GL_TEXTURE_2D);
	/*Combining env.*/
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	/*Fixing Deformations.*/
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glBindTexture(GL_TEXTURE_2D, model.sun_texture->getTextureID());
	GLUquadricObj *sun = gluNewQuadric();
	gluQuadricDrawStyle(sun, GLU_FILL); //quadric related.
	gluQuadricOrientation(sun, GLU_OUTSIDE); //quadric normals, from the center to the exterior.
	gluQuadricTexture(sun, true);
	gluQuadricNormals(sun, GLU_SMOOTH); //quadric related.
	gluSphere(sun, model.sun.radius, 10, 16);
	gluDeleteQuadric(sun);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

/*This methods draws the bootstraped locals.*/
void draw_poi() {
	vector <POI>::iterator poi_iterator;
	for (poi_iterator = model.poilist.begin(); poi_iterator != model.poilist.end(); poi_iterator++) {
		//poi_iterator->render(model.old_house, model.shader, model.light);
		if (poi_iterator->getModelHashtag() == BUILDING) {
			poi_iterator->render(model.building, model.shader, model.light);
		}
		else if (poi_iterator->getModelHashtag() == OLD_HOUSE) {
			poi_iterator->render(model.old_house, model.shader, model.light);
		}
		else if (poi_iterator->getModelHashtag() == GRASS) {
			poi_iterator->renderSphere(model.ball_texture);
		}
		else if (poi_iterator->getModelHashtag() == TREE) {
			poi_iterator->render(model.tree, model.shader, model.light);
		}
		else {
			poi_iterator->render(model.old_house, model.shader, model.light);
		}
	}
}

void draw_road() {
	vector <Road>::iterator road_iterator;
	for (road_iterator = model.roads.begin(); road_iterator != model.roads.end(); road_iterator++) {
		road_iterator->render();
	}
}

void draw_character() {
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glTranslatef(model.character.x, model.character.y, model.character.z);
	glScalef(0.02f, 0.02f, 0.02f);
	glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	glRotatef(model.character.turnSpeed, 0.0, 0.0, 1.0);
	mdlviewer_display(model.character.model);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void draw_2D_Text(string text, GLfloat x, GLfloat y) {
	//glColor3f(1.0, 1.0, 1.0);
	/*Must disable lighting and textures to use this.*/
	//glDisable(GL_LIGHTING);
	unsigned char *p = new  unsigned char[text.size() + 1];
	strcpy((char*)p, text.c_str());
	const char * string = text.c_str();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	model.view->setOrthogonal2DView();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	/*Raster Specific Position.*/
	glRasterPos2f(x, y);
	int textlength = text.size();
	//for (int i = 0; i < textlength; i++) {
	glutBitmapString(GLUT_BITMAP_8_BY_13, p);
	//freetype::print(our_font, x, y, string);
	glPopMatrix();
}

void draw_fps_counter() {

	if (state.fpscounter.start == false) {
		string text = "0.00";
	}
	ostringstream aux;
	aux << "FPS counter: " << state.fpscounter.frameRate;
	string text = aux.str();
	GLint w, h;
	model.view->size(w, h);
	GLfloat width = (GLfloat)w;
	GLfloat height = (GLfloat)h;
	/*TODO: calibrate this to each monitor.*/
	GLfloat x = width / 8.0f;
	GLfloat y = height - 100.0f;
	glColor3f(0.0f, 0.0f, 0.0f);
	draw_2D_Text(text, x, y);
}

void popup_notification(string note) {
	glColor3f(0.0f, 0.0f, 0.0f);
	string poi = "POI: ";
	int size = (int)note.size();
	string text = note.substr(1, size - 2);
	poi.append(text);
	GLint w, h;
	model.view->size(w, h);
	GLfloat x, y;
	x = (GLfloat)(w / 8);
	/*TODO: must do callibration.*/
	y = (GLfloat)(h - 120.0);
	draw_2D_Text(poi, x, y);
}

void draw_info_sidebar(GLboolean flag) {

	string text = "";
	string text_ = "";
	string enter = "";

	glPushMatrix();
	glLoadIdentity();
	if (flag == GL_TRUE) {
		glColor3f(0.0f, 0.0f, 0.0f);
		//text = "F key - Close Sidebar";
		text_ = "Path: " + model.chosen_path;
		enter = "Press Enter to run the selected Path.";
	}
	else {
		/*F key.*/
	}
	GLint w, h;
	model.view->size(w, h);
	GLfloat width = (GLfloat)w;
	GLfloat height = (GLfloat)h;
	GLfloat x = WIDTH_ON_TEXT(width) + 50.0f;
	GLfloat y = height - 50.0f;
	draw_2D_Text(text, x, y);

	GLfloat x_ = width / 10.0f;
	GLfloat y_ = 120.0f;
	draw_2D_Text(text_, x_, y_);

	GLfloat x_2 = width / 10.0f;
	GLfloat y_2 = 100.0f;
	draw_2D_Text(enter, x_2, y_2);

	glPopMatrix();

}

/*Draws the square reference to each POI.*/
void draw_square_reference(GLint w, GLint h, GLfloat x, GLfloat y, bool mark) {

	Path_quad_reference aux;

	/*Quads measurements.*/
	GLfloat x_aux = x - 30;
	GLfloat x_aux2 = x - 10;
	GLfloat y_aux = y + 10;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glPushMatrix();
	gluOrtho2D(0.0, w, 0.0, h);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();


	if (mark == false) {
		glColor3f(1.0, 1.0, 1.0);
	}
	else if (mark == true) {
		glColor3f(0.0, 0.0, 0.0);
	}
	glBegin(GL_QUADS);
	glVertex2f(x_aux, y);
	glVertex2f(x_aux, y_aux);
	glVertex2f(x_aux2, y_aux);
	glVertex2f(x_aux2, y);
	glEnd();


	glColor3f(0.0, 0.0, 0.0);
	glBegin(GL_LINE_LOOP);

	glVertex2f(x_aux, y);
	aux.p1x = x_aux;
	aux.p1y = y;

	glVertex2f(x_aux, y_aux);
	aux.p2x = x_aux;
	aux.p2y = y_aux;

	glVertex2f(x_aux2, y_aux);
	aux.p3x = x_aux2;
	aux.p3y = y_aux;

	glVertex2f(x_aux2, y);
	aux.p4x = x_aux2;
	aux.p4y = y;

	glEnd();

	glPopMatrix();
	glPopMatrix();

	/*pushing to set mouse clickable path.*/
	model.quads_reference_poi.push_back(aux);

}

void draw_POIs_name() {
	vector<POI> ::iterator it;
	int cont = 0;
	bool mark;
	float inc_value = 200.0;
	for (it = model.poilist.begin(); it != model.poilist.end(); it++) {
		string name = it->getName();
		GLint w, h;
		model.view->size(w, h);
		GLfloat x = (GLfloat)WIDTH_ON_TEXT(w) + 100.0f;
		GLfloat y = (GLfloat)h - inc_value;
		/*per each POI coordinate.*/
		if (model.path_number_ref == cont) {
			mark = true;
		}
		else {
			mark = false;
		}
		draw_square_reference(w, h, x, y, mark);
		draw_2D_Text(name, x, y);
		inc_value += 20.0;
		cont++;
	}
}

void draw_user_info(bool flag) {
	string text;
	glColor3f(0.0f, 0.0f, 0.0f);
	if (flag) {
		string username(model.user_char);
		text = "Logged as:";
		text.append(username);
	}
	else {
		text = "Free Roam Mode";
	}
	GLint w, h;
	model.view->size(w, h);
	GLfloat x, y;
	x = (GLfloat)(w / 8.0f);
	y = (GLfloat)(h - 140.0f);
	draw_2D_Text(text, x, y);
}

void draw_top_window() {
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	model.view->setOrthogonal2DView();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, model.label->getTextureID());
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex2f(state.width / 10 - 30.0f, state.height - 10.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex2f(state.width / 10 - 30.0f, state.height - 220.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex2f(state.width / 3.0f, state.height - 220.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex2f(state.width / 3.0f, state.height - 10.0f);
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glPopMatrix();
}

void draw() {
	//glutSetCursor(GLUT_CURSOR_NONE);
	//state.fpscounter.beginFrame = clock();
	///*For every draw it requires buffers clear.*/
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/*callback code*/

	/*glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();*/


	/*Setting up Camera position and where to look.*/

	//renders_sun_trajectory(10.0f);

	//GLfloat light_source_position[4];
	//light_source_position[0] = model.sun.x;
	//light_source_position[1] = model.sun.y;
	//light_source_position[2] = model.sun.z;
	//light_source_position[3] = 0.0f;

	///*Performs glulookat.*/
	/*model.camera->updateCamera(model.character.speed, model.character.turnSpeed, model.character.x, model.character.y,
		model.character.x, model.character.y, model.character.z);*/

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);


	/*Centering.*/
	if (active_viewport == 1) {
		skybox->render(model.character.x, model.character.y, model.character.z, 400, 400, 50);

	}

	//glFog();
	glPushMatrix();
	//draw_axis();
	draw_floor();
	draw_poi();
	draw_road();
	glPopMatrix();
	draw_character();
	//draw_sun();
	glPopMatrix();
	glDisable(GL_LIGHTING);

	/*Checks for character proximity.*/
	GLfloat *character_position = new GLfloat[3];
	character_position[0] = model.character.x;
	character_position[1] = model.character.y;
	character_position[2] = (GLfloat)model.character.z;


	if (active_viewport == 1) {

		printf("Debug activer_weather=%d\n", active_weather);
		if (active_weather == 1) {
			weather->drawSnow(model.character.x, model.character.y, model.character.z);
		}

		if (active_weather == 2) {
			weather->drawRain(model.character.x, model.character.y, model.character.z);
		}

		draw_top_window();
		string note_aux = Resources::stringWithAccentsChangedToNormalLetters(model.collider->checkCharacterPositionRelativeToPOI(character_position, model.poilist));
		if (note.size() == 0 && note_aux.size() != 0) {
			sound->PlayPoi();
		}
		note = note_aux;

		if (note.size() != 0) {
			popup_notification(note);
		}
		if (model.user_flag == true) {
			draw_user_info(model.user_flag);
		}
		else {
			draw_user_info(model.user_flag);
		}
		draw_fps_counter();

	}


	/*This is sidebars related.*/
	//if (model.projection.sidebarFlag == GL_TRUE) {
	//	model.view->setSidebar(-2.0f);
	//	/*Enables Cursor.*/
	//	//glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
	//	draw_info_sidebar(GL_TRUE);
	//	draw_POIs_name();
	//}
	//else if (model.projection.sidebarFlag == GL_FALSE) {
	//	model.view->setSidebar(2.0f);
	//	/*Disables.*/
	//	//glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
	//	draw_info_sidebar(GL_FALSE);
	//	/*Resets all Data.*/
	//	model.path_number_ref = -1;
	//	model.chosen_path = "";
	//}
	//state.fpscounter.endFrame = clock();
	//fps_counter();
	///*Swap - Double Buffer.*/
	//glutSwapBuffers();
}

void draw_intro() {
	glClear(GL_COLOR_BUFFER_BIT);

	GLfloat	menuVectors[4][3] =
	{
		{ -1.78,1,-1.72 },
		{ -1.78,-1,-1.72 },
		{ 1.78,-1,-1.72 },
		{ 1.78,1,-1.72 }
	};

	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, intro->getTextureID());
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f); glVertex3fv(menuVectors[0]);
	glTexCoord2f(0.0f, 1.0f); glVertex3fv(menuVectors[1]);
	glTexCoord2f(1.0f, 1.0f); glVertex3fv(menuVectors[2]);
	glTexCoord2f(1.0f, 0.0f); glVertex3fv(menuVectors[3]);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void draw_loading_viewport() {

	glViewport(0, 0, (GLsizei)width_f, (GLsizei)height_f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	model.view->setPerspectiveView(30, 0.01f, 1000.0f);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	model.camera->updateCamera(model.character.speed, model.character.turnSpeed, model.character.x, model.character.y,
		model.character.x, model.character.y, model.character.z);
	draw_intro();
	glutSwapBuffers();

}

void draw_first_viewport() {
	glScissor(0, 0, (GLsizei)width_f, (GLsizei)height_f);
	glViewport(0, 0, (GLsizei)width_f, (GLsizei)height_f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	model.view->setPerspectiveView(30, 0.01f, 1000.0f);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	model.camera->updateCamera(model.character.speed, model.character.turnSpeed, model.character.x, model.character.y,
		model.character.x, model.character.y, model.character.z);
	draw();

}

void draw_second_viewport() {
	glScissor((width_f / 4) * 3, (height_f / 4) * 3, width_f / 4, height_f / 4);
	glViewport((width_f / 4) * 3, (height_f / 4) * 3, width_f / 4, height_f / 4);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	model.view->setPerspectiveView(30, 0.01f, 9000.0f);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	model.camera2->updateCamera(model.character.speed, model.character.turnSpeed, model.character.x, model.character.y,
		model.character.x, model.character.y, model.character.z);
	draw();

}

void DisplayBothViewPorts() {
	state.fpscounter.beginFrame = clock();
	/*For every draw it requires buffers clear.*/
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_SCISSOR_TEST);
	active_viewport = 1;
	draw_first_viewport();
	glClear(GL_DEPTH_BUFFER_BIT);
	active_viewport = 2;
	draw_second_viewport();


	glDisable(GL_SCISSOR_TEST);

	state.fpscounter.endFrame = clock();
	fps_counter();

	/*Swap - Double Buffer.*/
	glutSwapBuffers();

}

void key(unsigned char key, int x, int y) {

	switch (key) {
	case 27:
		exit(1);
		break;

	case 13:
		model.go = true;
		break;

	case 'A':
	case 'a':
		model.keys.a = GL_TRUE;
		break;
	case'D':
	case 'd':
		model.keys.d = GL_TRUE;
		break;
	case 'W':
	case 'w':
		model.keys.w = GL_TRUE;
		break;
	case 'S':
	case's':
		model.keys.s = GL_TRUE;
		break;
	case 'F':
	case'f':
		if (model.keys.f == GL_TRUE) {
			model.keys.f = GL_FALSE;
		}
		else if (model.keys.f == GL_FALSE) {
			model.keys.f = GL_TRUE;
		}
		break;
	case 'M':
	case 'm':
		sound->ChangeVolume();
		break;
	case 'T':
	case 't':
		weather = new Weather();
		weather->init(model.character.x, model.character.y, model.character.z);
		if (active_weather == 2) {
			active_weather = 0;
		}
		else{
			active_weather++;
		}
		break;
	case 32:
		if (model.projection.orthogonalFlag == true) {
			model.projection.orthogonalFlag = false;
			model.projection.perspectiveFlag = true;
		}
		else if (model.projection.perspectiveFlag == true) {
			model.projection.perspectiveFlag = false;
			model.projection.orthogonalFlag = true;
		}
		break;
	}
}

void keyUp(unsigned char key, int x, int y) {
	switch (key) {

	case 'A':
	case 'a':
		model.keys.a = GL_FALSE;
		break;
	case'D':
	case 'd':
		model.keys.d = GL_FALSE;
		break;
	case 'W':
	case 'w':
		model.keys.w = GL_FALSE;
		break;
	case 'S':
	case's':
		model.keys.s = GL_FALSE;
		break;
	}
}

/*Select Path.*/
void mouse_clicked(int button, int estado, int x, int y) {
	vector<Path_quad_reference> ::iterator it;
	int cont = 0;

	if (button == GLUT_LEFT_BUTTON && estado == GLUT_DOWN) {
		/*Check inside each quad.*/
		for (it = model.quads_reference_poi.begin(); it != model.quads_reference_poi.end(); it++) {
			/*Mouse coordinates are reported reversed on the y axis.*/
			GLfloat new_y = (GLfloat)state.height - y;
			/*Therefore this must be also changed on the y validations.*/
			if (x > it->p1x && x < it->p3x && new_y < it->p2y && new_y > it->p1y) {
				model.chosen_path += " - " + model.poilist.at(cont).getName();
				/*Saves reference to clicked poi.*/
				model.path_number_ref = cont;
				/*saving selected point.*/
				model.path_list.push_back(model.poilist.at(cont));
				break;
			}
			cont++;
		}
	}
}

/*moving the Mouse while holding left button changes the camera position only*/
void mouse_motion(int x, int y) {

	if (x < state.mouse.previous_x) { //go up.
		//model.camera->incrementLongitude(0.75f);
	}
	else if (x > state.mouse.previous_x) {
		//model.camera->incrementLongitude(-0.75f);
	}

	if (y < state.mouse.previous_y) {
		//model.camera->incrementLatitude(0);
	}
	else if (y > state.mouse.previous_y) {
		//model.camera->incrementLatitude(0);
	}

	/*saves mouse position on the screen.*/
	state.mouse.previous_x = x;
	state.mouse.previous_y = y;

	glutPostRedisplay();
}

void mouse_wheel(int wheel, int direction, int x, int y) {
	if (model.projection.perspectiveFlag) {
		if (direction < 0) {
			model.camera->incrementDistance(2.0f);
		}
		if (direction > 0) {
			model.camera->incrementDistance(-2.0f);
		}
	}
	glutPostRedisplay();
}

/*This method is not being used atm.*/
void mouse_eyes(int x, int y) {

	if (x < state.mouse.previous_x) {
		if (model.character.turnSpeed > 360.0f) {
			model.character.turnSpeed = 0.0f;
		}
		model.character.turnSpeed += 0.5f;
	}
	else if (x > state.mouse.previous_x) {
		if (model.character.turnSpeed < -360.0f) {
			model.character.turnSpeed = 0.0f;
		}
		model.character.turnSpeed -= 0.5f;
	}

	if (y < state.mouse.previous_y) {
		model.camera->incrementLatitude(0.05f);

	}
	else if (y > state.mouse.previous_y) {
		model.camera->incrementLatitude(-0.05f);
	}

	/*Centers Mouse.*/
	int w, h;
	model.view->size(w, h);
	//cout << "width: " << w << "height" << h << endl;
	if (x <= 20) {
		glutWarpPointer((int)w / 2, (int)h / 2);
	}
	if (x >= (w - 20)) {
		glutWarpPointer((int)w / 2, (int)h / 2);
	}
	if (y <= 20) {
		glutWarpPointer((int)w / 2, (int)h / 2);
	}
	if (y >= (h - 20)) {
		glutWarpPointer((int)w / 2, (int)h / 2);
	}
	//cout << "this is x:"<< x << endl;
	/*saves mouse position on the screen.*/
	state.mouse.previous_x = x;
	state.mouse.previous_y = y;

	glutPostRedisplay();
}

/*Loads one Shader.*/
void loadShader() {

	Point light_source_position;
	light_source_position.x = 40.0f;
	light_source_position.y = 40.0f;
	light_source_position.z = 30.0f;
	Point light_colour;
	light_colour.x = 0.2f;
	light_colour.y = 0.2f;
	light_colour.z = 0.2f;
	model.light = new Light(light_source_position, light_colour);
	model.shader = new Shader("vertexShader.txt", "fragmentShader.txt", "position", "textureCoordinates", "normalCoordinates");
}

/*Loads Models and Textures.*/
void loadResources() {
	Data building;
	Data old_house;
	Data tree, stadium;

	tree = OBJFile::readObjFile("res/grass/grass.obj");
	building = OBJFile::readObjFile("res/building/building.obj");
	old_house = OBJFile::readObjFile("res/old_house/old_house.obj");

	model.ball_texture = TextureFactory::loadTexture("res/stadium/desporto2.tga");
	model.label = TextureFactory::loadTexture("res/porto-label/label.tga");
	model.building = VAOFactory::loadDatatoVAO("res/building/building.tga", building.data_disposed, building.texture_data_disposed, building.normal_data_disposed, building.data_indexed);
	model.old_house = VAOFactory::loadDatatoVAO("res/old_house/old_house.tga", old_house.data_disposed, old_house.texture_data_disposed, old_house.normal_data_disposed, old_house.data_indexed);
	model.tree = VAOFactory::loadDatatoVAO("res/grass/grass.tga", tree.data_disposed, tree.texture_data_disposed, tree.normal_data_disposed, tree.data_indexed);
}

/*This method closes the parent Window.*/
void close_window(Fl_Widget *o, void*) {
	Fl_Button * b = (Fl_Button*)o;
	Fl_Group * w = b->parent();
	w->hide();
}

void no_authenthication_window() {
	Fl_Window *window = new Fl_Window(400, 200);
	window->position(state.width / 2 - 200, state.height / 2 - 200);
	Fl_Box *box = new Fl_Box(0, 0, 400, 200, "No Authenthication.");
	box->box(FL_NO_BOX);
	box->labeltype(FL_ENGRAVED_LABEL);
	box->labelsize(20);
	window->end();
	window->show();
	//Fl::run();
}

void logout(Fl_Widget *o, void*) {
	model.user_flag = false;
	model.user_char = NULL;
	model.password_char = NULL;
	/*Clearing all Paths to the current user.*/
	Fl_Button * b = (Fl_Button*)o;
	Fl_Group * w = b->parent();
	w->hide();
}

void go(Fl_Widget *o, void*) {

	/*model.chosen_path = "";
	vector<POI> ::iterator it;
	for (it = model.path_list.begin(); it != model.path_list.end(); it++) {
		model.chosen_path += it->getName();
		model.chosen_path += "->";
	}
*/
//model.go = true; //sets the go flag to true; this sets up the compute path till the flag equals false by the compute path.
//hide.
	model.go = true;
	model.reached_destination = false;
	model.walking_on_poi = false;
	model.path_number = 0;
	Fl_Button * b = (Fl_Button*)o;
	Fl_Window * w = (Fl_Window*)b->parent();
	Fl_Window *n = Fl::next_window(w);
	n->hide();
	w->hide();

}

void show_path(Fl_Widget *o, void* param) {
	Fl_Button * b = (Fl_Button*)o;
	//Fl_Window *parent = (Fl_Window*) b->parent();
	int y = b->y(); //x button
	vector<Path> ::iterator it;
	vector<int> ::iterator it_int;
	vector<int> ::iterator pressed_iterator;

	int cont = 0;
	for (pressed_iterator = model.buttons_pressed.begin(); pressed_iterator != model.buttons_pressed.end(); pressed_iterator++) {
		if (*(pressed_iterator) == y) {
			break;
		}
		cont++;
	}

	int id_button = model.ids_pressed.at(cont);

	char *text = NULL;
	string path_str = "";
	string path_str_2;
	/*clear vec path everytime a new path is selected.*/
	model.path_list.clear();
	for (it = model.user_path_list.begin(); it != model.user_path_list.end(); it++) {
		int id_int = it->getID();
		if (id_int == id_button) {
			vector<int> path_vec = it->getPath();
			for (it_int = path_vec.begin(); it_int != path_vec.end(); it_int++) {
				POI *p = POI::getPOIbyID(*(it_int));
				model.path_list.push_back(*p);
				string id_str = p->getName();
				path_str += id_str;
				path_str += "\n";
			}
			break;
		}
	}

	size_t size = path_str.size();
	path_str_2 = path_str.substr(0, size - 1);
	text = new char[path_str_2.size() + 1];
	strcpy(text, path_str_2.c_str());

	int y_size = 400;
	int x_size = 500;
	Fl_Window *window = new Fl_Window(x_size, y_size);
	window->position(width_f / 2 - 200, height_f / 2 - 100);

	window->begin();
	Fl_Text_Display *path = new Fl_Text_Display(10, 20, x_size - 20, y_size - 70, "Path");
	Fl_Text_Buffer *buff = new Fl_Text_Buffer();
	path->buffer(buff);
	window->resizable(*path);
	buff->text(text);
	path->wrap_mode(Fl_Text_Display::WRAP_AT_PIXEL, 0);

	Fl_Button *close_button = new Fl_Button((x_size / 2) + 30, y_size - 35, 50, 30, "Close");
	close_button->labelsize(12);

	Fl_Button *go_button = new Fl_Button((x_size / 2) - 45, y_size - 35, 50, 30, "GO!");
	go_button->labelsize(12);
	window->end();
	/*Callbacks.*/
	close_button->callback(close_window);
	go_button->callback(go);
	window->show();
}

void make_http_request_paths(Fl_Widget *o, void*) {

	vector <Path> ::iterator it;
	string token_ = model.user->Token();

	char* token = new char[token_.length() + 1];
	strcpy(token, token_.c_str());

	string username_ = model.user->Username();
	char* username = new char[username_.length() + 1];
	strcpy(username, username_.c_str());

	const char* filename = "path_response.json";
	WebRequest::PathHttpRequest(filename, token, username);
	model.user_path_list = JSONParser::parsePathFile(filename);

	//access window.
	Fl_Button * b = (Fl_Button*)o;
	Fl_Group * w = b->parent();

	w->begin();
	int pos = 90;
	for (it = model.user_path_list.begin(); it != model.user_path_list.end(); it++) {
		//Fl_Window *window = new Fl_Window(10, pos, 380, 30); //creating a new window.
		//window->begin();
		string path_name = it->getName();
		int path_name_size = (int)path_name.size() - 2;
		path_name = path_name.substr(1, path_name_size);
		char *path_name_char = new char[path_name.length() + 1];
		strcpy(path_name_char, path_name.c_str());

		Fl_Button *button = new Fl_Button(10, pos, 380, 30, path_name_char);
		button->labelsize(12);
		button->callback(show_path);
		//window->add(button);
		//window->end();
		model.ids_pressed.push_back(it->getID());
		model.buttons_pressed.push_back(pos);
		pos = pos + 30;
		//buttons.push_back(*button);
		w->add(button);
	}
	w->end();
	w->redraw();
}

void render_path_menu() {
	int y_size = 800;
	int x_size = 400;
	Fl_Window *window = new Fl_Window(x_size, y_size);
	window->position(width_f / 2 + 200, 10);

	Fl_Box *title_lbl = new Fl_Box(0, 0, x_size, 30, "User Paths");
	window->begin();
	title_lbl->box(FL_NO_BOX);

	title_lbl->labelsize(18);
	title_lbl->labeltype(FL_SYMBOL_LABEL);

	Fl_Button *get_path = new Fl_Button(x_size / 2 - 40, 30, 80, 40, "Get Paths"); //Makes the request.
	get_path->labelsize(12);
	Fl_Button *close_button = new Fl_Button((x_size / 3) * 2 - 40, y_size - 35, 80, 30, "Close");
	close_button->labelsize(12);
	Fl_Button *logout_button = new Fl_Button(x_size / 3 - 10, y_size - 35, 80, 30, "Logout");
	logout_button->labelsize(12);
	window->end();

	/*Callbacks.*/
	get_path->callback(make_http_request_paths);
	logout_button->callback(logout);
	close_button->callback(close_window);

	window->show();
	Fl::run();
}

void make_login_http_request(Fl_Widget *o, void *param) {

	bool returned_user_flag = false;

	Fl_Input *username = (Fl_Input*)param;

	/*Getting values from the input box.*/
	string username_ = model.username->value();
	string password_ = model.password->value();

	const char* filename = "login_response.json";

	if (username_.size() != NULL && password_.size() != NULL) { //only changes the login if something was written in each input box.
		model.user_char = new char[username_.length() + 1];
		strcpy(model.user_char, username_.c_str());

		model.password_char = new char[password_.length() + 1];
		strcpy(model.password_char, password_.c_str());

		/*Performs new request.*/
		WebRequest::LoginHttpRequest(filename, model.user_char, model.password_char);
		model.user = JSONParser::parseLoginfile(filename);

		/*Checks if User is valid.*/
		if (model.user != NULL && model.user->Token().size() != NULL) {
			returned_user_flag = true;
		}
	}
	else { // if no info was written on each input box.
		fl_alert("No valid Username or Password");
	}

	/*If User is valid.*/
	if (returned_user_flag) {
		Fl_Button * b = (Fl_Button*)o;
		Fl_Group * w = b->parent();
		w->hide();
		//render_path_menu();
		model.user_flag = true; // this flag enables other options.
	}
	else {
		if (*(model.user_char) == NULL) { // if the value pointed by.
			model.user_flag = false;
		}
		else {
			// Keeping the previous user logged.
			no_authenthication_window();
		}
	}
}

void render_login_menu() {
	/*if (model.user_flag == true) {
		model.user_flag = false;
	}*/
	Fl_Window *window = new Fl_Window(400, 200);
	window->position(state.width / 2 - 200, state.height / 2 - 200);
	Fl_Box *title_lbl = new Fl_Box(0, 0, 400, 30, "Login:");
	Fl_Box *username_lbl = new Fl_Box(0, 30, 200, 30, "Username:");
	Fl_Box *password_lbl = new Fl_Box(0, 90, 200, 30, "Password:");
	window->begin();

	title_lbl->box(FL_NO_BOX);
	username_lbl->box(FL_NO_BOX);
	password_lbl->box(FL_NO_BOX);

	title_lbl->labelsize(16);
	title_lbl->labeltype(FL_SYMBOL_LABEL);
	username_lbl->labelsize(12);
	username_lbl->labeltype(FL_SYMBOL_LABEL);
	password_lbl->labelsize(12);
	password_lbl->labeltype(FL_SYMBOL_LABEL);

	model.username = new Fl_Input(150, 30, 200, 30);
	model.username->textsize(12);
	model.password = new Fl_Secret_Input(150, 90, 200, 30);
	model.password->textsize(12);
	model.username->tooltip("String input");
	model.password->tooltip("String input");

	Fl_Button *login_button = new Fl_Button(100, 160, 50, 30, "Login");
	login_button->labelsize(12);
	Fl_Button *close_button = new Fl_Button(270, 160, 50, 30, "Close");
	close_button->labelsize(12);
	window->end();

	/*Callbacks.*/
	close_button->callback(close_window);
	login_button->callback(make_login_http_request);

	window->show();
	Fl::run();
}

void close_window_with_glut(Fl_Widget *o, void *param) {
	Fl_Button * b = (Fl_Button*)o;
	Fl_Group * w = b->parent();
	w->hide();
	exit(0);
}

void render_error_window() {
	Fl_Window *window = new Fl_Window(400, 200);
	window->position(width_f / 2 - 200, height_f / 2 - 200);
	Fl_Box *title_lbl = new Fl_Box(0, 0, 400, 30, "Error");
	const char *error = "Connection Failed while trying to Request POI Information. \n Please connect to VPN service and run the Application again.";
	Fl_Box *err_lbl = new Fl_Box(0, 30, 400, 140, error);
	window->begin();

	title_lbl->box(FL_NO_BOX);
	title_lbl->labelsize(16);
	title_lbl->labeltype(FL_SYMBOL_LABEL);
	err_lbl->labelsize(12);
	err_lbl->labeltype(FL_SYMBOL_LABEL);

	Fl_Button *close_button = new Fl_Button(185, 140, 40, 30, "Close");
	close_button->labelsize(12);
	window->end();

	/*Callbacks.*/
	close_button->callback(close_window_with_glut);

	window->show();
	Fl::run();
}

void menu_handler(int op) {
	switch (op) {
	case 1:
		render_login_menu();
		break;
	case 2:
		if (model.user_flag) {
			render_path_menu();
		}
		break;
	case 3:
		if (model.user_flag) {
			model.user_flag = false;
		}
		break;
	case 4:
		exit(0);
		break;
	}

	glutPostRedisplay();
}


void reshape(int width, int height) {
	width_f = width;
	height_f = height;
	state.width = width;
	state.height = height;
	model.view->setViewport(0, 0, width, height);
	DisplayBothViewPorts();
}

int main(int arg, char **argv) {
	Resources::getMonitorResolution(width_f, height_f);
	alutInit(&arg, argv);
	glutInit(&arg, argv);
	glutInitWindowSize(width_f, height_f);
	glutInitWindowPosition(0, 0);
	/*Using Double Buffer.*/
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Porto Visitas 3D") == GL_FALSE) exit(1);
	#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")


	//glutDisplayFunc(draw_intro);
	//// enter full screen
	//glutGameModeString("1920x1080:32@60"); // 1º teste
	//if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE)) {
	//	glutEnterGameMode();
	//}
	//else {
	//	glutGameModeString("1366x768:32@60");
	//	if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
	//	{
	//		glutEnterGameMode();
	//	}
	//	else {
	//		glutGameModeString("1024x768:32@60"); // 1º teste
	//		if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE)) {
	//			glutEnterGameMode();
	//		}
	//		else
	//		{
	//			glutGameModeString("800x600:32@60"); // 2º teste
	//			if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
	//			{
	//				glutEnterGameMode();
	//			}
	//			else // Cria Janela Normal
	//			{
	//				glutInitWindowSize(800, 600);
	//				glutInitWindowPosition(0, 0);
	//				/*Using Double Buffer.*/
	//				glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//				if (glutCreateWindow("TuKPorto") == GL_FALSE)	exit(1);
	//			}
	//		}
	//	}
	//}

	//glutFullScreen();
	glewExperimental = GL_TRUE;
	glewInit();

	const char* filename = "pois_response.json";
	WebRequest::PoisHTTPRequest(filename);
	try {
		model.poilist = JSONParser::parseFile(filename);
	}
	catch (const invalid_argument &e) {
		render_error_window();
	}

	sound = new Sound();
	sound->init();

	loadShader();
	loadResources();

	glutReshapeFunc(reshape);
	glutTimerFunc(1, timer, 1); // de 1 em 1 milisegundo.
	//DisplayBothViewPorts()
	glutDisplayFunc(DisplayBothViewPorts);
	glutMouseFunc(mouse_clicked);
	glutMotionFunc(mouse_motion);
	glutMouseWheelFunc(mouse_wheel);
	//glutPassiveMotionFunc(mouse_eyes);
	//glutFullScreen();

	glutKeyboardFunc(key);
	glutKeyboardUpFunc(keyUp);
	//glutSpecialFunc(specialKey);
	//glutSpecialUpFunc(specialKeyUp);

	/*vertical fov can be as high as 180º
	aspect ratio with width and height
	znear placed near the camera*/
	model.view = new View();
	model.camera = new Camera(15.0, 15.0, false);
	model.camera2 = new Camera(50.0, 90.0, true);
	init();

	sound->PlayMain();
	glutMainLoop();

	VAOFactory::deleteData();
	model.shader->deleteShaders();
	return 0;

}