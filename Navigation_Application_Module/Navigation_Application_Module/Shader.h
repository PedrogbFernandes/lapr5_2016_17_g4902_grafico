#pragma once

#include "Resources.h"
class Shader
{
	
private:
	int programID;
	int vertexShaderID;
	int fragmentShaderID;
	static int loadShader(const char* file, int type);
	
protected:

	/*attribute is the number of the attribute list to bind onto this shader program.*/
	void bindAttributes(int attribute, const GLchar* variableShader);

public:
	Shader(const char* vertexShader, const char* fragmentShader, const char* positionVarName, const char* textureVarName, const char* normalVarName);
	~Shader();

	void start();
	void stop();
	void deleteShaders();
	int getProgramID();	
};

