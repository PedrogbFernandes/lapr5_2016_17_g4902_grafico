#include "Shader.h"

Shader::Shader(const char * vertexShader, const char * fragmentShader, const char* positionVarName, const char* textureVarName, const char* normalVarName) {
	this->vertexShaderID = loadShader(vertexShader, GL_VERTEX_SHADER);
	bindAttributes(0, positionVarName);
	bindAttributes(1, textureVarName);
	//bindAttributes(2, normalVarName);
	this->fragmentShaderID = loadShader(fragmentShader, GL_FRAGMENT_SHADER);
	this->programID = glCreateProgram();
	glAttachShader(this->programID, this->vertexShaderID);
	glAttachShader(this->programID, this->fragmentShaderID);
	glLinkProgram(this->programID);
	glValidateProgram(this->programID);
}

Shader::~Shader() {

}

void Shader::start() {
	glUseProgram(this->programID);
}

void Shader::stop() {
	glUseProgram(0);
}

void Shader::deleteShaders() {
	stop();
	glDetachShader(this->programID, this->vertexShaderID);
	glDetachShader(this->programID, this->fragmentShaderID);
	glDeleteShader(this->vertexShaderID);
	glDeleteShader(this->fragmentShaderID);
	glDeleteProgram(this->programID);
}

/*Loads a Shader from a specific text file.*/
int Shader::loadShader(const char* file, int type) {
	
	string line;
	string s;
	ifstream textfile(file);
	const GLchar* c;
	int j = 0;

	if (textfile.is_open()) {
		while (getline(textfile, line)) {
			if (line.size() != NULL) {
				s.append(line).append("\n");
				j++;
			}
		}
		textfile.close();
	}
	else {
		cout << " Could not Open Shader File" << endl;
	}

	/*Converts into const char* */
	c = s.c_str();
	int shaderID = glCreateShader(type);
	
	try {
		glShaderSource(shaderID, 1, &c, NULL);
	}
	catch (exception exc) {
		printf("Error");
	}
	glCompileShader(shaderID);
	GLint b = NULL;
	
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &b);
	
	/*Checking Shader Logs.*/
	if(b ==  GL_FALSE) {
		GLint maxLength = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(shaderID, maxLength, &maxLength, &errorLog[0]);
	/*	
		for (size_t i = 0; i < 150; i++){
			cout << errorLog[i];
		}*/
		glDeleteShader(shaderID);
	}
	return shaderID;
}

void Shader::bindAttributes(int attribute, const GLchar* variableShader) {
	glBindAttribLocation(this->programID, attribute, variableShader);
}

int Shader::getProgramID() {
	return this->programID;
}



