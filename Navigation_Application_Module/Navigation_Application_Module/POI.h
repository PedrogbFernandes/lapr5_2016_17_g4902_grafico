#pragma once

#include "Resources.h"
#include "VAO.h"
#include "Shader.h"
#include "Texture.h"

class POI 
{

private:
	static vector<POI> POI_Container;

	int id;
	string name;
	GLfloat latitude;
	GLfloat longitude;
	GLfloat radius;
	string description;
	
	GLfloat x, y, z;
	vector<int> connectedPoisID;
	vector<string> hashtags;
	void convert_units();

public:
	POI();
	POI(int ID, string name, GLfloat latitude, GLfloat  longitude, GLfloat z, string description);
	~POI();
	GLfloat getX();
	GLfloat getY();
	GLfloat getZ();
	GLfloat getRadius();
	string getDescription();
	string getName();
	void render(VAO *vao_file, Shader *shader, Light *light);
	void renderSphere(Texture *texture);
	void setConnections(vector<int> connections);
	vector<int> getConnections();
	void setHashtags(vector<string> hashtags);
	string getModelHashtag();
	int getID();

	static bool addPOI(POI poi);
	static POI* getPOIbyID(int id);


};

