#include "POI.h"

vector<POI> POI::POI_Container;

void POI::convert_units() {
	float y_parser_ = MAP_MAX_Y - MAP_MIN_Y;
	GLfloat y_parser = (GLfloat)abs(y_parser_);
	GLfloat latitudeConversion = (GLfloat)(this->latitude - MAP_MIN_Y);
	this->y = (GLfloat)(latitudeConversion * Y_MAX / y_parser);

	float x_parser_ = MAP_MAX_X - MAP_MIN_X;
	GLfloat x_parser = (GLfloat)abs(x_parser_);
	GLfloat longitudeConversion = (GLfloat)(this->longitude - MAP_MIN_X);
	this->x = (GLfloat)(longitudeConversion * X_MAX / x_parser);
}

POI::POI() {

}

POI::POI(int ID, string name, GLfloat latitude, GLfloat longitude, GLfloat z, string description) {
	this->id = ID;
	this->name = name;
	this->latitude = latitude;
	this->longitude = longitude;
	this->radius = 5;
	this->z = z;
	this->description = description;
	convert_units();
}

POI::~POI()
{
}

GLfloat POI::getX() {
	return this->x;
}

GLfloat POI::getY() {
	return this->y;
}

GLfloat POI::getZ() {
	return this->z;
}

GLfloat POI::getRadius() {
	return this->radius;
}

string POI::getDescription() {
	return this->description;
}

string POI::getName() {
	return this->name;
}

void POI::render(VAO *vao_file,Shader *shader, Light* light) {
	glPushMatrix();
	glTranslatef(this->x, this->y, this->z);
	glEnable(GL_COLOR_MATERIAL);

	/*Base*/
	glColor3f(0.2f, 0.3f, 0.5f);
	glPushMatrix();
	glBegin(GL_POLYGON);
	glNormal3f(0.0f, 0.0f, 1.0f);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f((GLfloat)(cos(i) * this->getRadius()), (GLfloat)(sin(i) * this->getRadius()), 0.0f);
	glEnd();
	glPopMatrix();
	
	/*Contornos.*/
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
	glNormal3f(0.0f, 0.0f, 1.0f);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f((GLfloat)(cos(i) * this->getRadius()), (GLfloat)(sin(i) * this->getRadius()), 0.0f);
	glEnd();	
	vao_file->render(*shader,*light);
	glDisable(GL_COLOR_MATERIAL);
	glPopMatrix();
}

void POI::renderSphere(Texture * texture) {
	GLfloat radius = 1.0f;

	glPushMatrix();
	glTranslatef(this->x, this->y, this->z);
	glEnable(GL_COLOR_MATERIAL);
	glColor3f(0.2f, 0.3f, 0.5f);

	glPushMatrix();
	glBegin(GL_POLYGON);
	glNormal3f(0.0f, 0.0f, 1.0f);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f((GLfloat)(cos(i) * this->getRadius()), (GLfloat)(sin(i) * this->getRadius()), 0.0f);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, radius);
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glBindTexture(GL_TEXTURE_2D, texture->getTextureID());
	GLUquadricObj *sphere = gluNewQuadric();
	gluQuadricDrawStyle(sphere, GLU_FILL);
	gluQuadricOrientation(sphere, GLU_OUTSIDE);
	gluQuadricTexture(sphere, true);
	gluQuadricNormals(sphere, GLU_SMOOTH);
	gluSphere(sphere, radius, 10, 16);
	gluDeleteQuadric(sphere);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
	/*Contornos.*/
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_LOOP);
	glNormal3f(0.0f, 0.0f, 1.0f);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f((GLfloat)(cos(i) * this->getRadius()), (GLfloat)(sin(i) * this->getRadius()), 0.0f);
	glEnd();
	glColor4f(1.0, 1.0, 1.0, 1.0);
	glPopMatrix();

	glDisable(GL_COLOR_MATERIAL);

	glPopMatrix();
}

void POI::setConnections(vector<int> connections) {
	this->connectedPoisID = connections;
}

vector<int> POI::getConnections() {
	return this->connectedPoisID;
}

void POI::setHashtags(vector<string> hashtags_) {
	this->hashtags = hashtags_;
}

string POI::getModelHashtag() {
	vector<string> ::iterator it;
	vector<string> ::iterator model;
	for (it = this->hashtags.begin(); it != this->hashtags.end(); it++) {
		if (*(it) == BUILDING || *(it) == OLD_HOUSE || *(it) == GRASS || *(it) == TREE) {
			return *it;
		}
	}
	return "";
}

int POI::getID() {
	return this->id;
}

bool POI::addPOI(POI poi) {
	vector<POI>::iterator it;
	for (it = POI_Container.begin(); it != POI_Container.end(); it++) {
		if (poi.getID() == it->getID()) {
			return false;
		}
	}
	POI_Container.push_back(poi);
	return true;
}

POI* POI::getPOIbyID(int id) {
	vector<POI>::iterator it;
	for (it = POI_Container.begin(); it != POI_Container.end(); it++) {
		if (id == it->getID()) {
			return &(*it);
		}
	}
	return NULL;
}